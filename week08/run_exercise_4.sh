gcc -c queue_lib.c && \
ar rcs libqueue.a queue_lib.o && \
gcc -c stack_lib.c && \
ar rcs libstack.a stack_lib.o && \
gcc -c exercise_4_postfix_parser.c && \
gcc -o exercise_4_postfix_parser exercise_4_postfix_parser.o -lqueue -lstack -L. && \
./exercise_4_postfix_parser