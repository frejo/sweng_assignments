#include "stack_lib.h"

snode _STACK_SENTINEL_ = {0, 0};

void initialize_stack(stack* s) {
    /* Set the stack to point to _STACK_SENTINEL_. */
    s->top_node = &_STACK_SENTINEL_;
}

void push(int x, stack* s) {
    /* Push the given value to the top of the stack */

    /* make a new snode with given value */
    snode* this_node = malloc(sizeof(snode));
    this_node->data = x;
    this_node->lower_node = s->top_node; /* The new snode should point to the last snode, which is currently the top of the stack */ 
    s->top_node = this_node; /* Make the new note top */
}

int pop(stack* s) {
    /* Remove the top item in the stack and return its value */

    /* make a pointer to the top snode, and store its value in a variable */
    snode* this_node = s->top_node;
    int this_value = this_node->data;

    /* If the stack isn't empty, set the top snode to the second top snode, and free the memory assigned to the top snode */
    snode* last_node = this_node->lower_node;
    if (stack_is_empty(s) == false) {
        s->top_node = last_node;
        free(this_node);
    } else {
        /* We cannot pop an empty stack */
        printf("Stack underflow\n");
    }

    return this_value;
}

bool stack_is_empty(stack* s) {
    /* Check whether the stack is empty */
    return s->top_node == &_STACK_SENTINEL_;
}

void print_stack(stack s) {
    /* Prints stack with the top first */
    
    snode* current_node = s.top_node;
    if (current_node == &_STACK_SENTINEL_) {
        printf("Stack empty\n");
        return;
    }
    printf("Stack: ");
    while (current_node != &_STACK_SENTINEL_) {
        int value = current_node->data;
        printf("%d ", value);
        current_node = current_node->lower_node;
    }
    printf("\n");
}
