/* Library for a queue of mixed ints and chars */


#ifndef _queue_lib_h
#define _queue_lib_h

#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <stdbool.h>

typedef enum {type_int, type_char} TYPE;
union queue_data {
    /* A node in the queue can either hold an int or a char */
    int i;
    char c;
};

typedef struct qnode {
    /* A node holds a type and either a char or int as value and a pointer the next node in the queue */
    TYPE val_type;
    union queue_data value;
    struct qnode* next_node;
} qnode;

typedef struct queue {
    qnode* first_node;
    qnode* last_node;
    u_int8_t size;   // Use an unsigned 8bit int for storing the size
} queue;

void initialize_queue(queue* q);

/* Int functions */
void enqueuei(int x, queue* q);
int dequeuei(queue* q);

/* Char functions */
void enqueuec(char x, queue* q);
char dequeuec(queue* q);

/* Universal functions */
void empty_queue(queue* q);
bool queue_is_empty(queue* q);
bool queue_is_full(queue* q);
void print_queue(queue* q);


#endif
