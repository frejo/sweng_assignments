#include "queue_lib.h"

qnode _QUEUE_SENTINEL_ = {0,0,0};

void initialize_queue(queue* q) {
    q->first_node = &_QUEUE_SENTINEL_;
    q->last_node = &_QUEUE_SENTINEL_;
    q->size = 0;
}

void enqueuei(int x, queue* q) {
    /* Add an integer to the end of the queue */
    if (queue_is_full(q) == false) {
        qnode* new_node = malloc(sizeof(qnode));
        
        qnode* last  = q->last_node;

        /* Set values of the new node */
        new_node->val_type = type_int;
        new_node->value.i = x;
        new_node->next_node = &_QUEUE_SENTINEL_;

        if (q->first_node == &_QUEUE_SENTINEL_) {
            /* When the stack is empty, the new node is be both the first and last */
            q->first_node = new_node;
            q->last_node  = new_node;
        } else {
            /* The alst node points the the new node, which is then made the last node */
            last->next_node = new_node;
            q->last_node = new_node;
        }
        q->size++;
    }
}

int dequeuei(queue* q) {
    /* Remove the item in the front of the queue and return it's integer value */
    int x;
    qnode* first;

    first = q->first_node;
    x = first->value.i;

    if (first != &_QUEUE_SENTINEL_) {
        q->first_node = first->next_node;
        free(first);
        if(q->first_node == &_QUEUE_SENTINEL_) {
            q->last_node = &_QUEUE_SENTINEL_;
        }

        q->size--;
    } else {
        printf("Queue underflow\n");
    }

    return x;
}

void enqueuec(char x, queue* q) {
    /* Add a character to the end of the queue */
    if (queue_is_full(q) == false) {
        qnode* new_node = malloc(sizeof(qnode));
        
        qnode* last  = q->last_node;

        /* Set values of the new node */
        new_node->val_type = type_char;
        new_node->value.c = x;
        new_node->next_node = &_QUEUE_SENTINEL_;

        if (q->first_node == &_QUEUE_SENTINEL_) {
            /* When the stack is empty, the new node is be both the first and last */
            q->first_node = new_node;
            q->last_node  = new_node;
        } else {
            /* The alst node points the the new node, which is then made the last node */
            last->next_node = new_node;
            q->last_node = new_node;
        }
        q->size++;
    }
}

char dequeuec(queue* q) {
    /* Remove the item in the front of the queue and return it's character */
    char x;
    qnode* first;

    first = q->first_node;
    x = first->value.c;

    if (first != &_QUEUE_SENTINEL_) {
        q->first_node = first->next_node;
        free(first);
        if(q->first_node == &_QUEUE_SENTINEL_) {
            q->last_node = &_QUEUE_SENTINEL_;
        }
        q->size--;
    }

    return x;
}

void empty_queue(queue* q) {
    /* Dequeue until the queue is empty */
    while (queue_is_empty(q) != true) {
        dequeuei(q);
    }
}

bool queue_is_empty(queue* q) {
    /* Return whetyyher the queue is empty */
    return (q->first_node == &_QUEUE_SENTINEL_ && q->last_node == &_QUEUE_SENTINEL_);
}

bool queue_is_full(queue* q) {
    /* check if the queue is less that 255 qnodes long, since the variable storing the size is 8 bit and cannot exceed 255 */
    return q->size == 255;
}

void print_queue(queue* q) {
    /* Print each item in the queue as either int or char as specified in the node */
    qnode* current_node = q->first_node;
    while (current_node != &_QUEUE_SENTINEL_) {
        if (current_node->val_type == type_int) {
            printf("%d ", current_node->value.i);
        } else if (current_node->val_type == type_char) {
            printf("%c ", current_node->value.c);
        } else {
            printf("UnkownTypeError ");
        }
        
        current_node = current_node->next_node;
    }
    printf("\n");
}