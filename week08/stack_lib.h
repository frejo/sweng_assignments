
#ifndef _STACK_LIB_H
#define _STACK_LIB_H

#include <stdbool.h>    
#include <stdlib.h>
#include <stdio.h>

typedef struct snode {
    int data;
    struct snode* lower_node;
} snode;

typedef struct stack {
    snode* top_node;
} stack;

void initialize_stack(stack* s);
void push(int x, stack* s);
int pop(stack* s);
bool stack_is_empty(stack* s);
void print_stack(stack s);

#endif // ndef _STACK_LIB_H