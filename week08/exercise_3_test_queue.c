
#include "queue_lib.h"
#include <assert.h>

void test_queue() {
    queue q;
    initialize_queue(&q);
    assert(queue_is_empty(&q) == true); // Test A

    /* Test ints in queue */

    int x, y;

    x = 9;
    enqueuei(x, &q);
    print_queue(&q);

    y = dequeuei(&q);
    
    assert(x == y);                      // Test B.i
    assert(queue_is_empty(&q) == true); // Test B.ii

    int x0, x1;
    int y0, y1;

    x0 = 5;
    x1 = 13;
    enqueuei(x0, &q);
    enqueuei(x1, &q);
    print_queue(&q);

    y0 = dequeuei(&q);
    y1 = dequeuei(&q);
    
    assert(x0 == y0);                     // Test C.i
    assert(x1 == y1);                    // Test C.ii
    assert(queue_is_empty(&q) == true); // Test C.iii




    /* Test Chars in Queue */

    char a0, a1;
    char b0, b1;

    a0 = 'a';
    a1 = '*';
    enqueuec(a0, &q);
    enqueuec(a1, &q);
    print_queue(&q);

    b0 = dequeuec(&q);
    b1 = dequeuec(&q);
    
    assert(a0 == b0);                     // Test C.i
    assert(a1 == b1);                    // Test C.ii
    assert(queue_is_empty(&q) == true); // Test C.iii




    /* Test mixed queue */
    int x2,y2;
    char a2,b2;

    a2 = 'G';
    x2 = 90;
    enqueuec(a2, &q);
    enqueuei(x2, &q);
    print_queue(&q);

    b2 = dequeuec(&q);
    y2 = dequeuei(&q);
    
    assert(a2 == b2);                     // Test C.i
    assert(x2 == y2);                    // Test C.ii
    assert(queue_is_empty(&q) == true); // Test C.iii

}


int main() {
    test_queue();
    
    return 0;
}

