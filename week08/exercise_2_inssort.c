
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

/* Linked list structs */
typedef struct node
{
    int value;
    struct node* next_node;
} node;

typedef struct list
{
    struct node* first_node;
} list;


node SENTINEL = {0,0};


void initialize(list* l) {
    l->first_node = &SENTINEL;
}

void add_item(int x, list* l) {
    /* Add item to the end of a list */
    node* new_node = malloc(sizeof(node));
    node* node_a;

    new_node->value = x;

    node_a = l->first_node;
    new_node->next_node = &SENTINEL;

    if (node_a == &SENTINEL) {
        l->first_node = new_node;
    } else {
        while(node_a->next_node != &SENTINEL) {
            node_a = node_a->next_node;
        }
        node_a->next_node = new_node;
    }
}

void insertion_sort(int x, list* l) {
    /* Add item to a list using intertion sort */
    node* new_node = malloc(sizeof(node));

    node* node_a;
    node* node_b;

    new_node->value = x;

    node_a = l->first_node;
    node_b = node_a->next_node;

    if (node_a == &SENTINEL) {
        /* When the list is empty, set the first node to the given value */
        new_node->next_node = &SENTINEL;
        l->first_node = new_node;
    } else if ( x <= node_a->value ) {
        /* When the given value is smaller than the first item, set the new item in the front */
        new_node->next_node = node_a;
        l->first_node = new_node;
    } else {
        /* When the value is bigger than the first item, 
            iterate through the list while it hasn't reached 
            the end and node_b is smaller than x */
        while (node_b != &SENTINEL && x > node_b->value) {
            node_a = node_b;
            node_b = node_a->next_node;
        }
        /* Add a node between the two values. node_b will be SENTINEL if at the end */
        node_a->next_node = new_node;
        new_node->next_node = node_b;
    }
}

void empty_list(list* l) {
    /* Frees the memory of all nodes in a list */
    node* current_node;
    node* next_node;
    current_node = l->first_node;

    while (current_node != &SENTINEL) {
        next_node = current_node->next_node;
        free(current_node);
        current_node = next_node;
    }

    /* point list to sentinel */
    l->first_node = &SENTINEL;
}

void print_linked_list(list* l) {
    node* current_node;
    node* next_node;

    current_node = l->first_node;
    next_node = current_node->next_node;

    while (current_node != &SENTINEL) {
        printf("%d ", current_node->value);
        current_node = next_node;
        next_node = current_node->next_node;
    }
    printf("\n");
}

void test_insertion_sort() {
    /* Should test all extreme cases:
        1) Add first value
        2) Add value smaller than first
        3) Add value to end of list 
        4) Add value between two numbers 
        5) Add value equal to another number */

    list l;
    node* current_node;
    node* next_node;

    int i = 0;
    int n = 5;
    int test_array[] = {5,1,9,3,5};
    int sorted_array[] = {1,3,5,5,9};

    initialize(&l);
    
    for (i = 0; i < n; i++) {
        insertion_sort(test_array[i], &l);
    }

    current_node = l.first_node;
    next_node = current_node->next_node;

    i = 0;
    while (current_node != &SENTINEL) {
        //printf("%d: %d\n", i, current_node->value);
        assert(current_node->value == sorted_array[i]);
        current_node = next_node;
        next_node = current_node->next_node;
        i++;
    }
    assert(i==n); // the while loop should have iterated the same number of times as the numbers in the list.

    empty_list(&l);

    assert(l.first_node == &SENTINEL);
}


int main() {
    /* Automatic tests */
    test_insertion_sort();

    /* Visual tests: */ 
    list unsorted;
    list sorted;

    initialize(&unsorted);
    initialize(&sorted);

    /* Make an unsorted list for comparison */
    add_item(5, &unsorted);
    add_item(1, &unsorted);
    add_item(9, &unsorted);
    add_item(3, &unsorted);
    add_item(5, &unsorted);

    printf("Unsorted: \n");
    print_linked_list(&unsorted);

    /* Sort the same numbers in a list */
    insertion_sort(5, &sorted);
    insertion_sort(1, &sorted);
    insertion_sort(9, &sorted);
    insertion_sort(3, &sorted);
    insertion_sort(5, &sorted);

    printf("Sorted: \n");
    print_linked_list(&sorted);

    /* Free the memory of all the nodes in the lists */
    empty_list(&sorted);
    empty_list(&unsorted);

    printf("After emptying list: \n");
    print_linked_list(&sorted);

}