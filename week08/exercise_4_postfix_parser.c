
#include "queue_lib.h"
#include "stack_lib.h"
#include <stdlib.h>     /* atoi */
#include <stdio.h>     /* scanf, printf */

void ParseManualInput(queue* q);
void Parse(char* expr, queue* q);
int Evaluate(queue* q);

void ParseManualInput(queue* q) {
    printf("Enter postfix expression \n> ");
    char inp;
    while (inp != '\n') {
        scanf("%c", &inp);
        enqueuec(inp, q);
    }    
}

void Parse(char* expr, queue* q) {
    char c;
    int i = 0;
    while (c != 0) {
        c = expr[i];
        enqueuec(c, q);
        i++;
    }
}

int Evaluate(queue* q) {
    /* Doesn't work with negative integers as input. '0 a - '  can be used instead of '-a' though */
    char c, c_next;
    stack s;            /* Evaluation stack */
    queue digits;       /* To store the digits of a multi digit number */
    int digit_count = 0;

    initialize_stack(&s);
    initialize_queue(&digits);

    /* Dequeue the first two chars */
    c = dequeuec(q);

    while (c != 0) { /* Until end of string */
        //printf("'%c'\n", c);
        if (c >= '0' && c <= '9') {
            /* If the character is a digit, add it to the digit queue */
            enqueuec(c, &digits);
            digit_count++;
        } else if (c == ' ') {
            if (digit_count != 0) {
                /* last item must have been an int */

                /* make a char array to move the digits to from the queue to be able to use the atoi() function */
                char* dig_array = calloc( 1+digit_count , sizeof(char) );

                /* Move digits from queue to array */
                for (int i = 0; i < digit_count; i++) {
                    dig_array[i] = dequeuec(&digits);
                }

                /* Convert char array to int */
                int result = atoi(dig_array);
                //printf("Pushed %d\n", result);
                push(result, &s);

                digit_count = 0;
            }
        } else if (c == '+' || c == '-' || c == '*' || c == '/') {
            /* Not int */
            /* Pop the two top items in the evaluation stack and use the given arithmetic operator */
            int result, x1, x2;
            x2 = pop(&s);
            x1 = pop(&s);
            switch (c) {
            case '+':
                result = x1 + x2;
                break;
            case '-':
                result = x1 - x2;
                break;
            case '*':
                result = x1 * x2;
                break;
            case '/':
                result = x1 / x2;
                break;
            }
            push(result, &s);
            //printf("%d %c %d = %d\n", x1, c, x2, result );
        } else {
            /* Invalid character */ 
        }

        /* Iterate */
        c = dequeuec(q);
    }
    return pop(&s);
}

void test_parse_evaluate() {
    char expr_1[] = "4 6 * 8 * 98 -";   // Test the given expression
    char expr_2[] = "4 6 8 * * 98 -";   // Test the given expression with changed order
    char expr_3[] = "128 2 2 2 + * /";  // Test expression with 
    queue eval_queue;
    initialize_queue(&eval_queue);

    Parse(expr_1, &eval_queue);
    assert(Evaluate(&eval_queue) == ((4*6) * 8) - 98 );

    empty_queue(&eval_queue);

    Parse(expr_2, &eval_queue);
    assert(Evaluate(&eval_queue) == (((6 * 8) * 4) - 98));

    empty_queue(&eval_queue);

    Parse(expr_3, &eval_queue);
    assert( Evaluate(&eval_queue) == ( 128 / ( (2+2) * 2) ));

    empty_queue(&eval_queue);
}

int main() {
    test_parse_evaluate();
    queue eval_queue;
    initialize_queue(&eval_queue);

    ParseManualInput(&eval_queue);
    printf("= %d\n", Evaluate(&eval_queue));
}