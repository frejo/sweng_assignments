#include "heapstack_lib.h"

node SENTINEL = {0, 0};

void initialize(stack* s) {
    /* Set the stack to point to SENTINEL. */
    s->top_node = &SENTINEL;
}

void push(int x, stack* s) {
    /* Push the given value to the top of the stack */

    /* make a new node with given value */
    node* this_node = malloc(sizeof(node));
    this_node->data = x;
    this_node->lower_node = s->top_node; /* The new node should point to the last node, which is currently the top of the stack */ 
    s->top_node = this_node; /* Make the new note top */
}

int pop(stack* s) {
    /* Remove the top item in the stack and return its value */

    /* make a pointer to the top node, and store its value in a variable */
    node* this_node = s->top_node;
    int this_value = this_node->data;

    /* If the stack isn't empty, set the top node to the second top node, and free the memory assigned to the top node */
    node* last_node = this_node->lower_node;
    if (empty(s) == false) {
        s->top_node = last_node;
        free(this_node);
    } else {
        /* We cannot pop an empty stack */
        printf("Stack underflow\n");
    }

    return this_value;
}

bool empty(stack* s) {
    /* Check whether the stack is empty */
    return s->top_node == &SENTINEL;
}

void print_stack(stack s) {
    /* Prints stack with the top first */
    
    node* current_node = s.top_node;
    if (current_node == &SENTINEL) {
        printf("Stack empty\n");
        return;
    }
    printf("Stack: ");
    while (current_node != &SENTINEL) {
        int value = current_node->data;
        printf("%d ", value);
        current_node = current_node->lower_node;
    }
    printf("\n");
}
