
#include "heapstack_lib.h"
#include <stdio.h>
#include <stdbool.h>
#include <assert.h>

bool test_heapstack_lib(void) {
    stack test_stack;

    /* Check (A) Initilize creates an empty stack */
    initialize(&test_stack);
    if (empty(&test_stack) != true) {
        printf("Stack not empty after initialization!\n");
        return false;
    }

    /* To check the stack is the same before and after the operations */
    node* top = test_stack.top_node;

    /* Check (B) Single push-pop */
    int x,y;
    x = 4;
    push(x, &test_stack);
    print_stack(test_stack);
    y = pop(&test_stack);
    print_stack(test_stack);
    if (x != y) {
        printf("Value error after push-pop sequence\n");
        return false;
    }
    if (test_stack.top_node != top) {
        printf("Stack is different after push-pop sequence\n");
        return false;
    }

    /* Check (C) double push-pop */
    int x0,x1,y0,y1;
    x0 = 17;
    x1 = -71;
    push(x0, &test_stack);
    print_stack(test_stack);
    push(x1, &test_stack);
    print_stack(test_stack);
    y0 = pop(&test_stack);
    print_stack(test_stack);
    y1 = pop(&test_stack);
    print_stack(test_stack);
    if (x0 != y1 || x1 != y0) {
        printf("Value error after double push-pop sequence\n");
        return false;
    }
    if (test_stack.top_node != top) {
        printf("Stack is different after double push-pop sequence\n");
        return false;
    }

    return true;
}

int main(void) {
    assert(test_heapstack_lib());
}
