
#ifndef _HEAP_STACK_LIB_H
#define _HEAP_STACK_LIB_H

#include <stdbool.h>    
#include <stdlib.h>
#include <stdio.h>

typedef struct node {
    int data;
    struct node* lower_node;
} node;

typedef struct stack {
    node* top_node;
} stack;

void initialize(stack* s);
void push(int x, stack* s);
int pop(stack* s);
bool empty(stack* s);
void print_stack(stack s);

#endif // ndef _HEAP_STACK_LIB_H