
#include "sine_lib.h"   // Taylor sine approximation
#include <stdio.h>      // printf
#include <assert.h>     // asserts
#include <stdbool.h>    // bools
#include <math.h>       // ANSI sin for comparison

#define PI 3.14159265359

bool compare_sine_functions(double min, double max_pi, double step, double offset, int precision, double max_diff) {
    // Test that the taylor series doesn't differ more than specified from the ANSI implementation.
    assert(precision > 0);
    printf("\n~~~~ Compare test ~~~~\n - MIN: %.1f PI\n - MAX: %.1f PI\n - STEP: %.3f\n - Taylor Precision: %d\n", 2*offset+ min, 2*offset+max_pi, step, precision);
    printf("~~~~~~~~~~~~~~~~~~~~~~\n");
    for (double i = min; i <= max_pi; i += step) {
        // Calculate the angle with offset
        double phi = offset*2*PI + PI*i;
        double diff;
        double ANSI_sine = sin(phi);
        double T_sine = taylor_sine(phi, precision);
        diff = ANSI_sine - T_sine;

        printf("phi = %.3f ; ANSI Sine: %.10f ; Taylor Sine: %.10f ; difference: %.10f\n", phi, ANSI_sine, T_sine, diff);

        if (diff < -max_diff || diff > max_diff) {
            return false;
        }
    }
    return true;
}

bool test_taylor_sine(void) {
    // Do the same tests with different offsets
    for (double i = 0; i < 100; i+= 10) {
        // Test between 0 and pi/2 with precision 4 and max diff 0.005
        if (compare_sine_functions(0, .5, 0.1, i, 4, 0.005) != true) {
            return false;
        }

        // from 0 to pi with precision 9
        if (compare_sine_functions(0, 1, 0.1, i, 9, 0.0000005) != true) {
            return false;
        }

        // from pi to 2pi with precision 9
        if (compare_sine_functions(0, 1, 0.1, i+.5, 9, 0.005) != true) {
            return false;
        }

        // from 0 to pi with precision 11
        if (compare_sine_functions(0, 1, 0.1, i, 11, 0.000000005) != true) {
            return false;
        }

        // from pi to 2pi with precision 11
        if (compare_sine_functions(0, 1, 0.1, i+.5, 11, 0.0025) != true) {
            return false;
        }

        // from pi to 2pi with precision 12 to show it becomes unprecise - probably due to overflows in factorial or power functions
        if (compare_sine_functions(0, 1, 0.1, i+.5, 12, 0.1) != true) {
            return false;
        }
    }
    return true;

    /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        The tests show that with a low precision taylor series, like 4, it only really compares well to the ANSI implementation at low values below .5 PI.
        At higher precision, like 9-11, it is equal to the ANSI implementation to at least 10 digits from 0 to 2.2 and to 7 digits at PI. From PI to 2 PI, the difference between the two implementations rises significantly. 
        Another note is that at a precision of 12+ is actually worse than 11, most likely due to an overflow in the factorial or power function. 
       ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
}

int main(void) {
    // for (int i = 0; i < 10; i++) {
    //     printf("%d!: %d\n", i, factorial(i) );
    // }
    // for (int i = 0; i < 10; i++) {
    //     printf("2^%d: %f\n", i, power(2, i) );
    // }
    // for (int i = 0; i < 10; i++) {
    //     printf("3.2^%d: %f\n", i, power(3.2, i) );
    // }
    //compare_taylor_ANSI_sine(0, 0.5, 0.1, 4);
    assert(test_taylor_sine());
    return 1;
}

