#include "sine_lib.h"

#define PI 3.14159265359

unsigned long factorial(int n) {
    assert(n >= 0);
    unsigned long fact = 1;
    for (int i = 0; n-i > 0; i++) {
        fact *= n-i;
    }
    return fact;
}

long double power(double x, int n) {
    assert(n >= 0);
    /* x^0 = 1, for n bigger than 0, multiply x n times */
    double result = 1;
    for (int i = 0; i < n; i++) {
        result *= x;
    }
    return result;
}

double taylor_sine(double x, int n) {
    /* implement your function here */

    /* Since sine is the same for each 2pi*i+x, we can find the equivalent angle between 0 and 2pi, which is more precise in the taylor function 
    
    angle = (rotations - full_rotations) * 2pi

    the number of rotations is x/2pi and the number of full rotations is the same, but rounded down. Thus the angle is equal to (x/2pi - floor(x/2pi)) * 2pi
    */
    double angle = (x/(2*PI) - (double) (int) (x/(2*PI))) * 2*PI;

    double sine = 0;
    for (int i = 0; i < n; i++) {
        /* The number sequence is 1,3,5,7... = 2*i + 1 */
        int j = 2*i + 1;
        double fract = power(angle, j) / factorial(j);
        /* i = 0,2,4 ... term is added, 
           i = 1,3,5 ... is subtracted */
        if (i%2 == 1) {
            sine -= fract;
        } else {
            sine += fract;
        }
    }
    return sine;
}
