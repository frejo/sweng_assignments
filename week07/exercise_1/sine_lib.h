
#ifndef _TAYLOR_SINE_LIB_H
#define _TAYLOR_SINE_LIB_H

#include <assert.h>

unsigned long factorial(int n); 
long double power(double x, int n);
double taylor_sine(double x, int n);

#endif
