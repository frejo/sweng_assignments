#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <assert.h>

int sum(int* numbers, int size) {
    int sum = 0;
    for (int i = 0; i < size; i++) {
        sum += *(numbers+i);
    }
    //printf("%d\n", sum);
    return sum;
}

bool test_sum(void) {
    int a[] = {1,2,3,4,5,6,7,8,9};
    int b[] = {-9,-8,-7,6,5,-4,3,-2,1};
    int c[] = {8,5,4,9,1,7,6,3,2};
    int d[] = {1,11,111,22,3,555,44,33};
    int e[] = {2,71,828,182,845,90,45,235,360,287,471,35,27};
    bool success = true;
    success = success && (sum(a, sizeof(a)/sizeof(a[0])) == 45);
    success = success && (sum(b, sizeof(b)/sizeof(b[0])) == -15);
    success = success && (sum(c, sizeof(c)/sizeof(c[0])) == 45);
    success = success && (sum(d, sizeof(d)/sizeof(d[0])) == 780);
    success = success && (sum(e, sizeof(e)/sizeof(e[0])) == 3478);
    return success;
}

void enter_array(int* numbers, int size) {
    printf("Enter %d numbers: ", size);
    for (int i = 0; i < size; i++) {
        scanf("%d", numbers+i);
    }
}

int main(void) {
    assert(test_sum());

    int size;
    printf("Enter length of array: ");
    scanf("%d", &size);
    if (size <= 0) {
        printf("Size cannot be zero or less.\n");
        return 1;
    }
    int* a = (int*) calloc(size, sizeof(int));
    enter_array(a, size);
    printf("The sum is %d\n", sum(a, size));
}
