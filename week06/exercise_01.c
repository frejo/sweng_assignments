#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <assert.h>

int max(int* numbers, int size) {
    int current_max = *numbers;
    for (int i = 0; i < size; i++) {
        if ( *(numbers+i) >= current_max ) {
            current_max = *(numbers+i);
        }
    }
    //printf("%d\n", current_max);
    return current_max;
}

bool test_max(void) {
    int a[] = {1,2,3,4,5,6,7,8,9};
    int b[] = {-9,-8,-7,6,5,-4,3,-2,1};
    int c[] = {8,5,4,9,1,7,6,3,2};
    int d[] = {1,11,111,22,3,555,44,33};
    int e[] = {2,71,828,182,845,90,45,235,360,287,471,35,27};
    bool success = true;
    success = success && (max(a, sizeof(a)/sizeof(a[0])) == 9);
    success = success && (max(b, sizeof(b)/sizeof(b[0])) == 6);
    success = success && (max(c, sizeof(c)/sizeof(c[0])) == 9);
    success = success && (max(d, sizeof(d)/sizeof(d[0])) == 555);
    success = success && (max(e, sizeof(e)/sizeof(e[0])) == 845);
    return success;
}

void enter_array(int* numbers, int size) {
    printf("Enter %d numbers: ", size);
    for (int i = 0; i < size; i++) {
        scanf("%d", numbers+i);
    }
}

int main(void) {
    assert(test_max());

    int size;
    printf("Enter length of array: ");
    scanf("%d", &size);
    if (size <= 0) {
        printf("Size cannot be zero or less.\n");
        return 1;
    }
    int* a = (int*) calloc(size, sizeof(int));
    enter_array(a, size);
    printf("The biggest number is %d\n", max(a, size));
    return 0;
}
