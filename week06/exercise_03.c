/* 
 * MaTriX file format .mtx:
 *  space seperated columns (and NO leading and trailing spaces)
 *  newline seperated rows (NO leading and trailing newlines)
 */

#include <stdio.h>
#include <stdlib.h>

/* Error codes */
#define ERR_READ_FILE               -1
#define ERR_WRITE_FILE              -2
#define ERR_BAD_MATRIX              -3
#define ERR_INCOMP_DIMENSIONS       -4

static char* filename_a = "matrix_a.mtx";
static char* filename_b = "matrix_b.mtx";
static char* filename_c = "matrix_c.mtx";

int count_cols(char* filename) {
    /* The number of columns should be the number of spaces on a line + 1 */
    FILE* file;
    file = fopen(filename, "r");
    if (file) {
        char c = '0';
        int cols = 1;
        /* Count spaces until end of file or until new line */
        while (fscanf(file, "%c", &c) == 1 && c != '\n') {
            //printf("%c", c);
            if (c == ' ') {
                cols++;
            }
        }
        //printf("\n");
        fclose(file);
        return cols;
    } else {
        return ERR_READ_FILE;
    }
}

int count_rows(char* filename) {
    /* The number of rows should be equal to the count of newline characters + 1 */
    FILE* file;
    file = fopen(filename, "r");
    if (file) {
        char c;
        int lines = 1;
        /* Scan each charater until the end of file, and increment counter if the char is a newline */
        while(fscanf(file, "%c", &c) == 1) {
            //printf("%c", c);
            if (c == '\n') {
                lines++;
            }
        }
        //printf("\n");
        return lines;
    } else {
        return ERR_READ_FILE;
    }
}


void multiply_matrixes(int* a, int* b, int* c, int rows_a, int cols_a, int cols_b) {
    /* The number of columns in a should equal the rows in b, so we only need to get one of those values passed along
     *
     * The matrixes are stored as 1d arrays, so to get cell (i,j) in a, use a[i * cols_a + j] ) 
     * 
     * Each cell in c, c(i,j), is the sum of a(i,k) * b(k,j) where k is each column in a.
     */
    for (int i = 0; i < rows_a; i++) {
        for (int j = 0; j < cols_b; j++) {
            c[i*cols_b+j] = 0;
            for (int k = 0; k < cols_a; k++) {
                //printf("i: %d, j: %d, k: %d\n", i,j,k);
                //printf("a_(i,k): %d, b_(k,j): %d\n\n", *(a+(i*cols_a+k)), *(b+(k *cols_b+j)));
                c[i*cols_b+j] += a[i*cols_a+k] * b[k *cols_b+j];
            }
        }
    }
}

int read_matrix(char* filename, int* a, int rows, int cols) {
    FILE* file;
    file = fopen(filename, "r");
    if (file) {
        int counter = 0; // ints read. Should be equal to i+1 and in the end rows*cols
        for (int i = 0; i < cols*rows; i++) {
            counter += fscanf(file, "%d", a+i); // If fscanf doesn't find more integers, it returns -1. This shouldn't happen if the file is made properly.
            //printf("read %d\n", *(a+i));
            //printf("%d\n", counter); 
        }
        if (counter != cols*rows) {
            return ERR_BAD_MATRIX;
        }
        return 0;
    } else {
        /* Couldn't open file for reading */
        return ERR_READ_FILE;
    }
}

int save_matrix(char* filename, int* a, int rows, int cols) {
    FILE* file;
    file = fopen(filename, "w");
    if (file) {
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                fprintf(file, "%d", a[i*cols+j]);
                if (j < cols - 1) {
                    fprintf(file, " ");
                }
            }
            if (i < rows - 1) {
                fprintf(file, "\n");
            }
        }
        fclose(file);
        return 0;
    } else {
        /* Couldn't open file for writing */
        return ERR_WRITE_FILE;
    }
}

void print_matrix(int* a, int rows, int cols) {
    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < cols; j++) {
            printf("%d ", a[i*cols+j]);
        }
        printf("\n");
    }
}

void file_error_message(int error_code, char* filename) {
    switch (error_code)
    {
    case ERR_READ_FILE:
        printf("Unable to open file '%s'. Make sure the file exists\n", filename);
        break;
    case ERR_WRITE_FILE:
        printf("Unable to save file '%s',\n", filename);
        break;
    case ERR_BAD_MATRIX:
        printf("Invalid matrix in file '%s'. Make sure there is no leading and trailing whitespaces and newlines\n", filename);
        break;
    default:
        printf("Unkown error loading file '%s'.\n", filename);
        break;
    }
}

int main(void) {
    int rows_a = count_rows(filename_a);
    int cols_a = count_cols(filename_a);
    int rows_b = count_rows(filename_b);
    int cols_b = count_cols(filename_b);
    int rows_c = rows_a;
    int cols_c = cols_b;

    if (rows_a < 0) {
        file_error_message(rows_a, filename_a);
        return rows_a;
    }
    if (cols_a < 0) {
        file_error_message(cols_a, filename_a);
        return cols_a;
    }
    if (rows_b < 0) {
        file_error_message(rows_b, filename_b);
        return rows_b;
    }
    if (cols_b < 0) {
        file_error_message(cols_b, filename_b);
        return cols_b;
    }

    printf("%dx%d, %dx%d\n", rows_a, cols_a, rows_b, cols_b);

    int* matrix_a = (int*) calloc(rows_a*cols_a, sizeof(int));
    int* matrix_b = (int*) calloc(rows_b*cols_b, sizeof(int));

    int err;
    err = read_matrix(filename_a, matrix_a, rows_a, cols_a);
    if (err < 0) {
        file_error_message(err, filename_a);
        return err; 
    }
    err = read_matrix(filename_b, matrix_b, rows_b, cols_b);
    if (err < 0) {
        file_error_message(err, filename_b);
        return err; 
    }

    printf("Matrix A:\n");
    print_matrix(matrix_a,rows_a,cols_a);
    printf("Matrix B:\n");
    print_matrix(matrix_b,rows_b,cols_b);

    if (cols_a == rows_b) {
        // everything's fine
    } else if (cols_b == rows_a) {
        rows_c = rows_b;
        cols_c = cols_a;
    } else {
        printf("Incompatible dimensions.\n");
        return ERR_INCOMP_DIMENSIONS;
    }

    int* matrix_c = (int*) calloc(rows_c*cols_c, sizeof(int));
    
    if (cols_a == rows_b) {
        multiply_matrixes(matrix_a, matrix_b, matrix_c, rows_a, cols_a, cols_b );
    } else if (cols_b == rows_a) {
        multiply_matrixes(matrix_b, matrix_a, matrix_c, rows_b, cols_b, cols_a );
    }

    printf("Matrix C:\n");
    print_matrix(matrix_c,rows_c,cols_c);

    err = save_matrix(filename_c, matrix_c, rows_c, cols_c);
    if (err < 0) {
        file_error_message(err, filename_c);
        return err;
    }

    return 0;
}
