/* 
 * Assignment week 1, exercise 3:
 * Write a program that adds two numbers; 
 *   write a program that adds three numbers; 
 *   write a program that adds four numbers.
 * 
 * Author: Tobias Frejo Rasmussen
 */

#include <stdio.h>

int w, x, y, z, sum, count;

int main(void) {
    printf("How many numbers do you want to add together?\n(2-4) > ");
    scanf("%d",&count);
    
    if (count > 1 && count <= 4) {
        printf("First number> ");
        scanf("%d", &w);
        printf("Second number> ");
        scanf("%d", &x);
        if (count > 2) {
            printf("Third number> ");
            scanf("%d", &y);
            if (count > 3) {
                printf("Fourth number> ");
                scanf("%d", &z);
            }
        }
    } else {
        printf("Bad input\n");
        return 1;
    }

    sum = w + x + y + z;
    printf("SUM = %d\n", sum);
    return 0;
}