/* 
 * Assignment week 1, exercise 1:
 * Write a program that swaps two numbers. 
 *  Given input 143 271 it produces 271 143.
 * 
 * Author: Tobias Frejo Rasmussen
 */

#include <stdio.h>

int x, y;

int main(void) {
    printf("Enter two numbers: ");
    scanf("%d%d", &x, &y);

    printf("Original numbers: %d %d\n", x, y);

               /* Example: x = 101, y = 110 (binary) */
    x = x ^ y; /*          101 xor 110 = 011         */
    y = x ^ y; /*          011 xor 110 = 101 (x)     */
    x = x ^ y; /*          011 xor 101 = 110 (y)     */

    printf("Swapped numbers:  %d %d\n", x, y);
    return 0;
}