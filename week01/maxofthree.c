/* 
 * Assignment week 1, exercise 4 (challenge):
 * 
 * Write a program that computes the maximum number 
 *  that can be produced by adding at most three 
 *  arbitrary (possibly negative) integer numbers.
 * 
 * Author: Tobias Frejo Rasmussen
 */

#include <stdio.h>

int x, y, z, max;
int sum = 0;
int x_used, y_used, z_used;

int main(void) {
    printf("Enter three numbers: ");
    scanf("%d%d%d", &x, &y, &z);

    /* If a number is positive, it will raise the sum, 
     *  and if it's negative, it will lower the sum. 
     * We only want to add the positives to the sum.
     */
    if (x >= 0) {
        sum = sum + x;
        x_used = 1;
    }
    if (y >= 0) {
        sum = sum + y;
        y_used = 1;
    }
    if (z >= 0) {
        sum = sum + z;
        z_used = 1;
    }

    if (sum == 0) {
        /* 
         * If the sum equals 0, it means that all the
         *  numbers are either negative or 0. In that
         *  case, we only want to return the single
         *  largest value.
         */
        sum = x;
        x_used = 1;
        if (y > sum) {
            sum = y;
            x_used = 0;
            y_used = 1;
        }
        if (z > sum) {
            sum = z;
            x_used = 0;
            y_used = 0;
            z_used = 1;
        }
    }

    printf("Input set of numbers: [%d, %d, %d]\n", x,y,z);
    printf("Subset holding the maximum sum: [");

    /* 
     * Print each number if it has been used. If one 
     *  of the following numbers also has been used, 
     *  print a ',' to seperate the items 
     */
    int total_used = x_used + y_used + z_used;
    if (x_used > 0) {
        printf("%d", x);
        if ((y_used + z_used) > 0) 
            printf(", ");
    }
    if (y_used > 0) {
        printf("%d", y);
        if (z_used > 0) 
            printf(", ");
    }
    if (z_used > 0) {
        printf("%d", z);
    }
    printf("]\n");
    printf("The sum of the subset = %d\n", sum);
}