/* 
 * Assignment week 1, exercise 2:
 * Write a program that computes the maximum of two integer numbers.
 * 
 * Author: Tobias Frejo Rasmussen
 */

#include <stdio.h>

int number_0, number_1, max;

int main(void) {
    printf("Enter two numbers: ");
    scanf("%d%d", &number_0, &number_1);
    
    if (number_1 > number_0)
        max = number_1;
    else
        max = number_0;

    printf("The maximum value is: %d\n", max);
    return 0;
}