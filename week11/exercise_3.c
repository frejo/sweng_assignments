/* As this is has a tail recursive function, remember to build with -O2 flag */

#include <assert.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>

/* Sum integers 1 to n */
unsigned long sum_rec(int n, unsigned long running_sum) {
    /* pre-condition */
    assert(n >= 1);

    /* post-condition */
    if (n > 1)
        return sum_rec(n - 1, n + running_sum);
    else
        return 1 + running_sum;
}

unsigned long sum_it(int n) {
    unsigned long sum;

    /* pre-condition */
    assert(n >= 1);

    sum = 0;

    /* post-condition */
    while (n > 0) {
        sum += n;
        n--;
    }
    return sum;
}

unsigned long sum_single(int n) {
    /* We actually don't need any loops or recursive function for this 
    
        Since multiplication is a computationally heavy operation, this will likely be slower for smaller numbers, but for big numbers, it wil very likely be faster. 
    */
    unsigned long sum;

    /* pre-condition */
    assert(n >= 1);

    /* post-condition */
    if (n % 2 == 0) {
        sum = (unsigned long)(n + 1) * (n / 2);
    } else {
        sum = (unsigned long)(n + 1) * (n / 2) + (n + 1) / 2;
    }
    return sum;
}

bool test_sum(void) {
    int n[] = {1, 5, 7, 10, 100, 1000000};
    unsigned long expected[] = {1, 15, 28, 55, 5050, 500000500000};
    for (int i = 0; i < 6; i++) {
        //printf("%d\n", n[i]);
        assert(sum_single(n[i]) == expected[i]);
        assert(sum_it(n[i]) == expected[i]);
        assert(sum_rec(n[i], 0) == expected[i]);
    }
    return true;
}

int main(void) {
    assert(test_sum());
    int n; 
    /* Assign the return value of scanf to an unused variable to get rid of the warning when using -O2 flag about an unused result: 
        "warning: ignoring return value of ‘scanf’, declared with attribute warn_unused_result [-Wunused-result]"
    */
    int unused = scanf("%d", &n);

    printf("S:   %lu\n", sum_single(n));
    printf("It:  %lu\n", sum_it(n));
    printf("Rec: %lu\n", sum_rec(n, 0));

    return 0;
}