#include <assert.h>
#include <stddef.h>
#include <stdio.h>

int sum_uneven(int n) {
    assert(n >= 1);

    if (n == 1) {
        return 1;
    } else {
        return (2 * n - 1) + sum_uneven(n - 1);
    }
}

int main(void) {
    int expected[] = {1,4,9,16,25,36,49,64,81,100};
    for (size_t i = 1; i < 10; i++) {
        int sum = sum_uneven(i);
        printf("%d\n", sum);
        assert(sum == expected[i-1]);
    }
    
    return 0;
}