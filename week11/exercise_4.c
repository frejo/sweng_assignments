/* As this is has a tail recursive function, remember to build with -O2 flag */

#include <assert.h>  /* assert */
#include <stdbool.h> /* bool */
#include <stdio.h>   /* printf */

int fib(int n, int x_0, int x_1) {
    /* x_0 = fib(x), 
       x_1 = fib(x+1) */

    /* pre-condition */
    assert(n >= 1);

    /* post-condition */
    int x_2 = x_0 + x_1; /* x_2 = fib(x)+fib(x+1) = fib(x+2) */

    if (n == 1) {
        return x_0;
    } else if (n == 2) {
        return x_1;
    } else {
        return fib(n - 1, x_1, x_2);
    }
}

int main(void) {
    int n;
    scanf("%d", &n);

    printf("%d\n", fib(n, 1, 1));

    return 0;
}