/*
 * Week 4, exercise 1:
 * Write a program that reverses the order of elements in an array, 
 * and then prints the contents of the reversed array.  For example,
 *  given the array:int a[5] = {1,2,3,4,5};...you should end up with 
 * another array b that has the elements:  [5,4,3,2,1].  Your program 
 * would then print the following to the console:
 *  5 4 3 2 1
 * 
 */

/*
 * Pre condition:
 *  Input of 5 ints to store in an array, a[5]
 * 
 * Post condition:
 *  b holds the contents of a, in reverse order
 * 
 * Method:
 *  for loop, from 0 to 4
 *      b[4-i] = a[i]
 * 
 */

#include <stdio.h>      // scanf(), printf() 
#include <stdbool.h>    // bools, true, false
#include <assert.h>     // assert()
#include <stdlib.h>     // rand()

#define ARRAY_LENGTH 5

int a[ARRAY_LENGTH];
int b[ARRAY_LENGTH];

/* Declare functions to implement later */
bool test(void);                    // Run algorithm to test the function
bool test_reverse(void);            // Tests if array b is the reverse of array a
void reverse(void);                 // Reverse array a into b
int input_array(void);              // Manually fill array a
void print_array_a(char seperator); // Print array a
void print_array_b(char seperator); // Print array b

int main(void) {
    /* Make three tests of random arrays */
    assert(test());

    input_array();
    reverse();

    print_array_a(',');
    print_array_b(',');
}

/* Implement functions */
int input_array(void) {
    printf("Enter %d numbers > ", ARRAY_LENGTH);
    for (int i = 0; i < ARRAY_LENGTH; i++) {
        scanf("%d", &a[i]);
    }
}

void print_array_a(char seperator) {
    printf("Array a: ");
    for (int i = 0; i < ARRAY_LENGTH; i++) {
        printf("%d", a[i]);
        if (i != ARRAY_LENGTH-1)
            printf("%c", seperator);
        else
            printf("\n");
    }
}

void print_array_b(char seperator) {
    printf("Array b: ");
    for (int i = 0; i < ARRAY_LENGTH; i++) {
        printf("%d", b[i]);
        if (i != ARRAY_LENGTH-1)
            printf("%c", seperator);
        else
            printf("\n");
    }
}

bool test(void) {
    /* To test the program, fill the array with random numbers, reverse 
     * the array and test id it actually has been properly reversed */
    for (int i = 0; i < ARRAY_LENGTH; i++) {
        a[i] = rand();
    }
    reverse();
    printf("Testing a random array:\n");
    print_array_a(',');
    print_array_b(',');
    
    if (test_reverse()) {
        printf("Test succeeded!\n\n");
        return true;
    }
    return false;
}

bool test_reverse(void) {
    bool is_reversed = true; 
    for (int i = 0; i < ARRAY_LENGTH; i++) {
        /* If two values doesn't match up, is_reversed will become false  */
        is_reversed = is_reversed && (b[ARRAY_LENGTH-1-i] == a[i]);
    }
    return is_reversed;
}

void reverse(void) {
    /* 
     * a[i_a] = b[i_b], where i_a = i and i_b = ARRAY_LENGTH-1-i
     * e.g. a[0] = b[4]
     *      a[1] = b[3]
     *          ...
     *      a[4] = b[0]
     */
    for (int i = 0; i < ARRAY_LENGTH; i++) {
        b[ARRAY_LENGTH-1-i] = a[i];
        // printf("%d,%d: %d,%d\n", i, ARRAY_LENGTH-1-i, a[i], b[ARRAY_LENGTH-i]);
    }
}