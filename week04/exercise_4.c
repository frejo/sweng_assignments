#include <stdio.h>

#define NN 20

int max(int x, int y) {
    return (x >= y) ? x : y;
}

void func(int a[], int N) {
    int r = 0;
    int n = 0;
    int s = 0;
    int h;
    while (n != N) {
        h = n;
        while (h != s) {
            if (a[h - 1] != a[n])
                h--;
            else s = h;
        }
        r = max(r, n + 1 - s);
        n++;
    }
    printf("%d\n", r);
}

void print_array(int arr[], int arr_len) {
    for (int i = 0; i < arr_len; i++) {
        printf("%d ", arr[i]);
    }
    printf("\n");
}

void main(void) {
    int a[NN] = { 1,1,2,2,3,3,4,5,6,9,1,1,7, 3, 4, 2, 11,1,9,10 };
    print_array(a, sizeof(a)/sizeof(a[0]));
    for (int i = 0; i <= NN; i++) {
        printf("%2d: %2d, out: ", i, a[i-1]);
        func(a, i);
    }
}

// The program computes the count of different integers in a 
// subsequence of the array with most different integers 
// and no back to back integers of the same value.