/* 
 * Week 4, exercise 3
 * 
 * Write  a  program  that  counts  the  occurrences  of  numbers 
 *  between  1  to  20  that are stored in a two-dimensional array
 *  of size 100×150,  and record the counts in another
 *  (one-dimensional) array that has size 20.  Test your program
 *  by filling your two-dimensional  array  with  various  numbers 
 *  (e.g. via  some  interesting for loop patterns, or random number 
 *  generation). 
 *  Print the one-dimensional final counts tothe console.
 * 
 */

/* 
 * Method/pseudo code:
 * a is a 2d-array
 * b is a 1d array to count occurences of numbers in a
 * 
 * go though each row and column and write a random number to each cell
 *     a[x][y] = random int between 1 and 20
 * 
 * go though each row and column to reach all cells and increment 
 *  the counter at the right index of b
 *      b[ a[x][y] - 1 ]++
 * 
 * TEST:
 * have a ocurrance_test_array for test
 *      count in ocurrance_test_array when generating a number.
 * then compare the ocurrance_test_array with the occurance_array to
 *  make sure the occurrance array has been counted properly.
 * 
 */

#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <assert.h>
#include <time.h> // Time for rand seed

#define COLS 100
#define ROWS 150
#define MIN_INT 1
#define MAX_INT 20
#define RANGE MAX_INT-(MIN_INT-1)

int big_array[COLS][ROWS];
int occurrences[RANGE];

int occurrences_test[RANGE];


void fill_big_array(void) {
    // Use for loop and rand() to fill the big array and count occurrences in occurrences_test array
    srand(time(0)); // Use current time as seed for rand()
    for (int y = 0; y < ROWS; y++) {
        for (int x = 0; x < COLS; x++) {
            int random_number = rand()%RANGE + MIN_INT; //with the range [1,20] this gives a random number between 0 and 19, which we change to 1 to 20 by adding MIN_INT
            big_array[x][y] = random_number;
            occurrences_test[random_number-MIN_INT]++;
        }
    }
}

void count_occurrences(void) {
    //go though each row and column to reach all cells: b[ a[x][y] - 1 ]++
    for (int y = 0; y < ROWS; y++) {
        for (int x = 0; x < COLS; x++) {
            occurrences[big_array[x][y] - MIN_INT]++;
        }
    }
}

void print_occurrence_array(void) {
    // prints each cell in the occurrences array
    int total = 0;
    for (int i = 0; i < RANGE; i++) {
        printf("Number of %2d's: %d\n", i+MIN_INT, occurrences[i]);
        total += occurrences[i];
    }
    assert(total = ROWS*COLS); // secondary test to make sure the total number of occurences equals the number of cells in the table
    printf("Total cells: %d\n", total);
}

bool test(void) {
    //compare results from occurrences_test and occurrences arrays
    bool is_equal = true;
    for (int i = 0; i < RANGE; i++) {
        is_equal = (occurrences[i] == occurrences_test[i]);
    }
    return is_equal;
}

int main(void) {
    fill_big_array();
    count_occurrences();

    assert(test());

    print_occurrence_array();
    return 0;
}