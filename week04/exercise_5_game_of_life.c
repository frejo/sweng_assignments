/*
 * Challenge: 
 * The Game of Life is a cellular automaton (it looks a bit 
 * like a boardgame) that demonstrates how very simple rules 
 * applied on a large scale can createcomplexemergentpatterns.
 * It was invented by a mathematician John Conway.  Theidea 
 * is that the universe in the game of life is a checkerboard 
 * (we can suppose it is afixed size ofn×n), we call each 
 * square on the board acell.  Each cell is either alive 
 * (represented by∗) or dead (represented by−).  Cells interact 
 * with theirneighbours,which are the eight surrounding cells 
 * (above, below, left, right, upper-left, etc.).  Oneach turn 
 * (called ageneration), whether a cell is alive or not can 
 * change accordingto the following rules:
 * Rule 1: An alive cell with less than two alive neighbours dies 
 *         (under population)
 * Rule 2: An alive cell  with exactly two or  three alive  
 *         neighbours stays  alive,  andlives on into the next 
 *         generation (survival)
 * Rule 3: An alive cell with more than three alive neighbours dies 
 *         (over population)
 * Rule 4: A dead cell with exactly three alive neighbours becomes 
 *         a live cell (repro-duction)
 */

#include <stdio.h>      // printf(), scanf()
#include <stdbool.h>    // bool, true, false
#include <assert.h>     // assert()
#include <unistd.h>     // sleep()

// 22x25 is neccessary for being able to show the pattern die hard
#define COLS 22 
#define ROWS 25

#define LIVE_CHAR '*'
#define DEAD_CHAR '-'

/*
 *  COORDINATES FOR LIVE BLOCKS IN PREMADE MODELS
 */

// Still life, "Block"
const int block[][2] = {
    {1,1}, {1,2}, 
    {2,1}, {2,2}
};

// Still life, "Bee Hive"
const int beehive[][2] = {
          {2,1},{3,1},
    {1,2},            {4,2},
          {2,3},{3,3}

};

// Oscillator "Pulsar" with a period of 3
const int pulsar[][2] = {
            {4,1},  {5,1},  {6,1},              {10,1}, {11,1}, {12,1},

    {2,3},                      {7,3},      {9,3},                          {14,3},
    {2,4},                      {7,4},      {9,4},                          {14,4},
    {2,5},                      {7,5},      {9,5},                          {14,5}, 
            {4,6}, {5,6}, {6,6},                {10,6}, {11,6}, {12,6},

            {4,8}, {5,8}, {6,8},                {10,8}, {11,8}, {12,8},
    {2,9},                      {7,9},      {9,9},                          {14,9},
    {2,10},                     {7,10},     {9,10},                         {14,10},
    {2,11},                     {7,11},     {9,11},                         {14,11}, 

            {4,13}, {5,13}, {6,13},             {10,13}, {11,13}, {12,13}
};

// Oscillator "Penta-decathlon" with a period of 15
const int pentadecathlon[][2] = {
    {4,2},{5,2},{6,2},
    {4,3},      {6,3},
    {4,4},{5,4},{6,4},
    {4,5},{5,5},{6,5},
    {4,6},{5,6},{6,6},
    {4,7},{5,7},{6,7},
    {4,8},      {6,8},
    {4,9},{5,9},{6,9}
};

// Spaceship "Glider"
const int glider[][2] = {
                {2,0},
    {0,1},      {2,1},
          {1,2},{2,2}
};

// Stays alive for 129 generations. Needs 22x25 board
const int diehard[][2] = {
                                        {14, 5},
    {8, 6}, {9, 6},
            {9, 7},             {13, 7}, {14, 7}, {15, 7}
};

char board[COLS][ROWS];
char next_board[COLS][ROWS];
//int neighbor_count[COLS][ROWS];   // For debugging

void fill_board(void) {
    /* Manually fill board by taking input chars and storing them in the array */
    printf("How big a board do you want to fill? [x <= %d and y <= %d]> ", COLS, ROWS);
    int manual_cols, manual_rows;
    scanf("%d%d", &manual_cols, &manual_rows);
    assert(manual_cols > 0 && manual_cols <= COLS);
    assert(manual_rows > 0 && manual_rows <= ROWS);

    printf("Fill the board with the dimensions %d x %d\n", manual_cols, manual_rows);
    printf("using only %c for dead and %c for alive\n", DEAD_CHAR, LIVE_CHAR);
    printf("      ");
    for (int i = 0; i < manual_cols; i++) 
        printf("|");
    printf("  \n");
    for (int i = 0; i < manual_rows; i++) {
        printf("%3d | ", i+1);
        for (int j = 0; j < manual_cols; j++) {
            /* Only recognise * and - while ignoring any other chars inputted (eg. new line) */
            do {
                scanf("%c", &board[j][i]);
            } while (!(board[j][i] == DEAD_CHAR || board[j][i] == LIVE_CHAR));
        }
    }
}

void print_board(void) {
    /* Print the board with line numbers */
    for (int i = 0; i < ROWS; i++) {
        printf("%3d | ", i+1);
        for (int j = 0; j < COLS; j++) {
            printf("%c", board[j][i]);
        }
        /* 
         *  Print number of neighbors to each cell for debugging purposes
         *  printf("\t");
         *  for (int j = 0; j < COLS; j++) {
         *      printf("%d", neighbor_count[j][i]);
         *  }
         */
        printf("\n");
    }
}

bool test_survival(int n, int m) {
    // Rule 1 - under population:    < 2 neighbors = dead
    // Rule 2 - survival:              2 neighbors + self alive = alive
    // Rule 3 - over population:     > 3 neighbors = dead
    // Rule 4 - reproduction        == 3 neighbors = alive

    bool alive = (board[n][m] == LIVE_CHAR);
    int neighbors = 0;
    /* Go through each neighbor to a specified cell, and count the number of live ones */
    for (int x = -1; x < 2; x++) {
        for (int y = -1; y < 2; y++) {
            if (!(x == 0 && y == 0) &&      // A field is not it's own neighbor
                n+x >= 0 &&                 // make sure the checked field is in the board
                m+y >= 0 && 
                n+x < COLS && 
                m+y < ROWS &&  
                (int) board[n+x][m+y] == (int) LIVE_CHAR )    // Check whether the neighbor is alive)
                neighbors++;
        }
    }

    if (neighbors == 3 || (alive && neighbors == 2)) {
        /* A cell will only be alive if rule 2 or rule 4 is true.
         * So either there is exactly three neighbors or the cell itself 
         * is alive and there is exactly two neighbors
         */
        return true;
    } else {
        return false;
    }
}

void calc_next_gen(void) {
    /* Calculates the next generation, and storing it in the next_board array */
    for (int i = 0; i < ROWS; i++) {
        for (int j = 0; j < COLS; j++) {
            if (test_survival(j, i)) {
                next_board[j][i] = LIVE_CHAR;
            } else {
                next_board[j][i] = DEAD_CHAR;
            }
        }
    }
}

void next_gen_is_current_gen(void) {
    /* Copies the next_board array to the main board array */
    for (int i = 0; i < ROWS; i++) {
        for (int j = 0; j < COLS; j++) {
            board[j][i] = next_board[j][i];
        }
    }
}

void iterate_board(void) {
    /* Calculates the next generation and put it into the board array */
    calc_next_gen();
    next_gen_is_current_gen();
}

void run(int iterations) {
    /* Run the specified number of generations with visible output */
    printf("Simulating %d generations\n", iterations);
    for (int i = 1; i <= iterations; i++) {
        printf("Generation %d\n", i);
        iterate_board();
        print_board();
        usleep(250000); /* Sleep for .25 seconds between iterations */
    }
}

void clear_board(void) {
    /* Kill all cells on the board */
    for (int i = 0; i < ROWS; i++) {
        for (int j = 0; j < COLS; j++) {
           board[j][i] = DEAD_CHAR;
        }
    }
}

void make_model_offset(const int live_coords[][2], int array_length, int x_offset, int y_offset) {
    /* Put a model onto the board with an offset */

    /* Go through all coordinates in the array specifying the model and set 
     * each coordinate on the board to ALIVE */
    for (int i = 0; i < array_length; i++) {
        board[live_coords[i][0]+x_offset][live_coords[i][1]+y_offset] = LIVE_CHAR;
    }
}

void make_model(const int live_coords[][2], int array_length) {
    /* Put a model into the board with an offset of (0, 0) */
    make_model_offset(live_coords, array_length, 0, 0);
}

bool equality_test(void) {
    /* Compares if board and next_board are equal */
    bool is_equal = true;
    for (int i = 0; i < ROWS; i++) {
        for (int j = 0; j < COLS; j++) {
            is_equal = is_equal && (board[j][i] == next_board[j][i]);
        }
    }
    return is_equal;
}

bool tests(void) {
    bool correct = true;
    int array_length;
    /* Still life test */
    clear_board();
    array_length = sizeof(beehive)/sizeof(beehive[0]);
    make_model(beehive, array_length);
    calc_next_gen(); /* Calculates the next generation and store it in the next_board array without updating the board array */
    /* board holds gen 0 and next_board holds gen 1. They should be the same */
    correct = correct && equality_test();

    /* Oscillator test 
     *
     * Test the oscillator 'Pulsar' which has a period of 3,
     *  so gen 3 should be equal to gen 0
     */
    clear_board();
    array_length = sizeof(pulsar)/sizeof(pulsar[0]);
    make_model(pulsar, array_length);
    /* Iterate 3 generations */
    for (int i = 0; i < 3; i++) {
        iterate_board();
    }
    /* Put the original model in board, while the iterated version still is in next_board */
    clear_board();
    array_length = sizeof(pulsar)/sizeof(pulsar[0]);
    make_model(pulsar, array_length);

    correct = correct && equality_test();

    /* Spaceship test 
     *
     * Test the spaceship 'Glider' which takes a period of 4 to move 1 in both x and y.
     */
    clear_board();
    array_length = sizeof(glider)/sizeof(glider[0]);
    make_model(glider, array_length);
    /* Iterate 2 generations */
    for (int i = 0; i < 4; i++) {
        iterate_board();
    }
    /* Put the original model in board offset by 1 in x and 1 in y directions, while the iterated version is still in next_board */
    clear_board();
    array_length = sizeof(glider)/sizeof(glider[0]);
    make_model_offset(glider, array_length, 1, 1);

    correct = correct && equality_test();
    return correct;
}

int select_model(void) {
    int suggested_iterations;
    printf("Predefined patterns:\n");
    printf("[1] Block           (still life)\n");
    printf("[2] Bee hive        (still life)\n");
    printf("[3] Pulsar          (oscillator)\n");
    printf("[4] Pentadecathlon  (oscillator)\n");
    printf("[5] Glider          (space ship)\n");
    printf("[6] Die hard        (has 130 generations)\n");
    printf("[7] Manual          (make one yourself)\n");
    printf("\nSelect a pattern [0-7] > ");
    int selected = 0;
    scanf("%d",&selected);
    switch (selected)
    {
    case 1:
        make_model(block, sizeof(block)/sizeof(block[0]));
        suggested_iterations = 1;
        break;
    case 2:
        make_model(beehive, sizeof(beehive)/sizeof(beehive[0]));
        suggested_iterations = 1;
        break;
    case 3:
        make_model(pulsar, sizeof(pulsar)/sizeof(pulsar[0]));
        suggested_iterations = 3;
        break;
    case 4:
        make_model(pentadecathlon, sizeof(pentadecathlon)/sizeof(pentadecathlon[0]));
        suggested_iterations = 15;
        break;
    case 5:
        make_model(glider, sizeof(glider)/sizeof(glider[0]));
        suggested_iterations = 20;
        break;
    case 6:
        make_model(diehard, sizeof(diehard)/sizeof(diehard[0]));
        suggested_iterations = 130;
        break;
    case 7:
        fill_board();
        suggested_iterations = 10;
        break;
    default:
        printf("Invalid input. Selecting manual pattern.");
        fill_board();
        suggested_iterations = 10;
        break;
    }
    return suggested_iterations;
}

int main(void) {
    assert(tests());
    /* Initialize area 
     *
     * Either call the fill_board() function to manually set the board
     * or set the board to a predefined model using make_model().
     * 
     * !REMEMBER to set the board size macros accordningly!
     */
    clear_board();
    //fill_board();

    //int array_length = sizeof(pulsar)/sizeof(pulsar[0]); /* Calculate number of coordinates in the array */
    //make_model(pulsar, array_length);

    int suggested_iterations = select_model();

    printf("Your initial universe:\n");
    print_board();
    int iterations = 4;
    printf("How many iterations do you want to simulate? Enter 0 to use recommended (%d) > ", suggested_iterations);
    scanf("%d", &iterations);
    if (iterations == 0) {
        iterations = suggested_iterations;
    }
    run(iterations);
    return 0;
}