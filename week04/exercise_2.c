/*
 * Week 4, exercise 2:
 * 
 * Suppose I have a sequence of numbers such as:
 * 1,7,43,4,67,0,3,2,0,0,3,2,0,0,0,3,2,6
 * The longest sequence of zeroes has 3 consecutive zeroes.  
 * Write a functionthat computes the start index of the 
 * longest sequence of zeros in an array. To testyour program, 
 * you may use a variableathat is an integer array that you 
 * already fill up with integers, 
 * e.g. int a[13] = { 0, 0, 0, 4, 5, 0, 0, 0, 0, 0, 11, 0, 0 };
 * 
 */

/* 
 * Precondition: 
 *      Input of array.
 * 
 * Postcondition:
 *      an int variable holds the value of the index where the 
 *      longest consecutive row of zeroes in the array is. 
 * 
 * Method/pseudo code:
 *      counter = 0         number of cosecutive zeroes
 *      index = 0           index of first 0 of current set of zeroes
 *      max_index = 0       the index of the first zero in the biggest *                            set of consecutive zeroes
 *      max_count = 0       the number of most consecutive zeroes
 * 
 *      Go through the array:
 *          If the integer is a 0 {
 *              If the counter is 0 {  
 *                  save the index in index
 *              }
 *              increment counter
 *          } Else {
 *              if counter is bigger than max_count{
 *                  max_count = counter
 *                  max_index = index
 *              }
 *              set counter to 0
 *          }
 */

#include <stdio.h>      //printf(), scanf()
#include <stdbool.h>    //bools, true, false
#include <assert.h>     //assert()

#define ARRAY_LENGTH 15

int a[ARRAY_LENGTH];
int max_count;
int max_index;
int test_array[ARRAY_LENGTH] = {1,2,0,0,4,5,0,7,0,0,0,0,8,9,0}; // index of 0000 = 8
int test_index = 8;

int find_longest_consecutive_zeroes_index(int arr[]) {
    /*  */
    int count = 0;
    int index = 0;
    max_count = 0;
    max_index = 0;

    for (int i = 0; i < ARRAY_LENGTH; i++) {
        if (arr[i] == 0) {
            if (count == 0) {
                index = i;
            }
            count++;
        } else {
            if (count > max_count) {
                max_count = count;
                max_index = index;
            }
            count = 0;
        }
    }
    return max_index;
}

bool test_find_longest_consecutive_zeroes_index(void) {
    int returned_index = find_longest_consecutive_zeroes_index(test_array);
    //printf("%d\n",returned_index);
    return (returned_index == test_index);
}

void manual_input(void) {
    printf("Enter %d numbers: ", ARRAY_LENGTH);
    for (int i = 0; i < ARRAY_LENGTH; i++) {
        scanf("%d", &a[i]);
    }
}

void print_array(void) {
    printf("Your array with the index marked:\n[");
    for (int i = 0; i < ARRAY_LENGTH; i++) {
        if (i == max_index) {
            printf("*");
        }
        printf("%d", a[i]);
        if (i != ARRAY_LENGTH-1) {
            printf(", ");
        }
    }
    printf("]\n");
}

int main(void) {
    assert(test_find_longest_consecutive_zeroes_index());

    manual_input(); // Pupulates array a with manually inputted ints

    find_longest_consecutive_zeroes_index(a);
    printf("Count of most consecutives zeroes: %d at index %d\n", max_count, max_index);
    print_array();

    return 0;
}
