/* 
 * Week 3, Exercise 3,
 * 
 * Recall the definition of the Fibonacci numbers
 *     f1 = 1
 *     f2 = 2
 *     f_n = f_n−1 + f_n−2    (n ≤ 3)
 * Given two numbers a and b, calculate how many Fibonacci numbers 
 * are in the range[a, b]. 
 * 
 * Input
 * The input contains several ranges. Each range consists of two
 * non-negativeinteger numbers a and b, specified on a separate 
 * input line. Input is terminated by a = b = 0; otherwise, 
 * a ≤ b ≤ 10^18. The numbers a and b are given with no superfluous 
 * leading zeros. 
 * 
 * Output
 *     For each test case output on a single line the number of 
 *     Fibonacci numbers f_i with a ≤ f_i ≤ b.
 * 
 * Example
 *     10 100
 *     5
 *     1234567890 9876543210
 *     4
 *     0 0
 */

/*
 * 
 * Precontion: Input a and b, >= 0, a <= b <= 10^18 
 *              Repeating until a = 0, b = 0
 * 
 * Postcondtion: Print the number of fibonacci numbers f_i, where a <= f_i <= b.
 * 
 * Method:
 *      loop while a is not 0 and b is not 0 : 
 *          get input a and b
 *          loop through the fibonacci numbers from f_1 = 1, f_2 = 2 ... until f_i is bigger than b
 *              if f_i <= b:
 *                  if f_i >= a:
 *                      add 1 to counter
 *          print counter
 * 
 */

#include <stdio.h>
#include <assert.h>
#include <math.h>

unsigned long a, b;       /* Inputs */
unsigned long f_x, f_y;   /* Fibonacci numbers */
int n;          /* Counter */

int main(void) {
    while (1) {
        scanf("%lu%lu", &a, &b);

        assert(a <= pow(10,18));
        assert(b <= pow(10,18));

        if (a == 0 && b == 0) 
            return 0;
        
        int i = 1; 
        n = 0;
        while (f_x <= b) {
            if (i <= 2) {
                /* Start conditions: When i = 1 or 2, f_x = i */
                f_y = i-1;
                f_x = i;
            } else {
                /* At this point, 'f_x' stores f_n-1 and 'f_y' stores f_n-2 */
                f_x = f_x + f_y; /* f_n  = f_n-2 + f_n-1 */
                f_y = f_x - f_y; /* f_n-1 = f_n  - f_n-2 */
            }

            if (f_x >= a && f_x <= b) {
                n++;
            }

            i++;
        }

        printf("%d, %d\n", i, n);
        f_x = 1;
        f_y = 1;
    }
}
