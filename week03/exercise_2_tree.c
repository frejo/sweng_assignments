/* 
 * Week 3, Exercise 2,
 * 
 * Write a program that produces the following output printing one * at 
 * a time using loops discussed in the lecture.
 *      *
 *     ***
 *    *****
 *   *******
 *  *********
 * ***********
 *      *
 *      *
 */

/*
 * Pre condition:
 * height of tree and base known
 * 
 * Post condition: 
 * 
 * 1st line:    5 space, 1 * 
 * 2nd line:    4 space, 3 *
 * 3rd line:    3 space, 5 *
 * 4th line:    2 space, 7 *
 * 5th          1 space, 9 *
 * 6th          0 space, 11 *
 * 7th + 8th:   5 space, 1 *
 * 
 * space count equals   height - line number
 *  star count equals   2 * i + 1,  where i_1 = 0
 * 
 * 
 * ~~~~ pseudo code ~~~~
 * 
 * draw_tree function, args: tree_height, base_height:
 *      call function to draw a triangle with tree_height
 *      call function to draw a base fitting a triangle of tree_height, 
 *       with it's own height of basse_height
 * 
 * draw_triangle function, args: height:
 *      loop for lines:
 *          loop for spaces, count: height - line_number
 *          loop for stars, count: 2 * (i-1) + 1
 * 
 * draw_base function, args: tree_height, base_height
 *      loop for lines, equal to height:
 *          print (tree_height - 1) spaces and a *
 * 
 */

#include <stdio.h>
#include <assert.h>

int i,j,k; /* Loop variables */

int n = 6; /* Height of tree */
int m = 2; /* Height of base */

void draw_triangle(int height) {
    /* Make a triangle with *'s */

    /* Loop through all the lines */
    for (i = 1; i <= height; i++) {
        /* print (height - line_number) spaces */
        for (j = height - i; j > 0; j--) {
            printf(" ");
        }
        /* Print (2 * (line_number-1) + 1) , where 1 is subtracted from
         * the line_number to fulfill the function described at line 44 */
        for (k = 0; k < 2 * (i-1) + 1; k += 1) {
            printf("*");
        }
        printf("\n");
    }
}

void draw_base(int tree_height, int base_height) {
    /* Make base_height lines */
    for (i = 1; i <= base_height; i++) {
        /* Print (tree_height - 1) spaces to distribute the base properly */
        for (j = 1; j <= tree_height - 1; j++) {
            printf(" ");
        }
        printf("*\n");
    }
}

void draw_tree(int tree_height, int base_height) {
    assert(tree_height > 0);
    assert(base_height > 0);
    
    draw_triangle(tree_height);
    draw_base(tree_height, base_height);
}

int main(void) {
    draw_tree(6, 2);
    int t, b;
    scanf("%d%d", &t, &b);
    draw_tree(t, b);
    return 0;
}