/* 
 * Week 3, exercise 6,
 * 
 * The following program should compute all prime numbers 
 * smaller or equal to n.
 * Correct the program and confirm that it has the desired behavior.
 * 
 * 
 * 
 * This method of calculating primes is called Trial Division.
 * From Wikipedia: "This method divides n by each integer from 2 up to 
 * the square root of n. Any such integer dividing n evenly establishes 
 * n as composite; otherwise it is prime." 
 * Source: https://en.wikipedia.org/wiki/Prime_number#Trial_division
 * 
 * The original code snippet divides i by each interger from 2 to the square root of i, as it should, but it doesn't correctly check whether it divides evenly, and stops incrementing j if the remainder is bigger than 1. This is fixed by changing the second condition of the while loop to when the remainder is non-zero.
 */

#include <stdio.h>

void main(void) {
    int i, j, n;
    scanf("%d", &n);
    for (i = 2; i <= n; i++) {
        j = 2;
        while (j <= i/j && i%j != 0) // 'i%j == 1' changed to 'i%j != 0'
            j++;
        if (j > i/j) 
            printf("%d ", i);
    }
}