/* 
 * Week 3, exercise 5,
 * 
 * The following program computes integer numbers q and r such that 
 * q = m / n and r = m % n.
 * What are the intermediate values of q and r?
 * Test the program so that all statements are executed at 
 * least once in one of the test cases.
 * 
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * 
 * To get into the first loop, r (m) needs to be equal or bigger 
 * than b (n).
 * To get into the second loop, b can't be equal to n, thus it needs to 
 * have been through the fist loop. 
 * To get into the if statement, r (m) needs to be bigger than or 
 * equal to b, which we can be sure it is at least once, when we are 
 * in the loops.
 * 
 * Depending on the input, 3 things can happen:
 *  1)  m >= n, and we enter the first loop, the second loop and the 
 *      if statement, thus every line has run.
 *  2)  n < m, in which case none of the code in the loops will run
 *  3)  n = 0 and m >= 0, in which case the first loop will run 
 *      forever, since b = b * 2, where b = 0, will never be able 
 *      to get to another value than 0. 
 * 
 * So to be able to test the function and execute each line at least 
 *  once, we just need to test a case where m >= n.
 */

#include <stdio.h>
#include <assert.h>
#include <stdbool.h>

/* 
 * Print intermediate values
 */
void print_intermediate(int pos, int q, int r) {
    printf("Position %d:    q: %d, r: %d\n", pos, q, r);
}


/*
 * A way of calculating q = m / n and r = m % n
 */
void func(int m, int n, int* rq, int* rr) {
    printf("m: %d, n: %d\n", m, n);
    int q = 0;
    int r = m;
    int b = n;
    print_intermediate(0, q, r);
    while (r >= b) {
        print_intermediate(1, q, r);
        b *= 2;
    }
    while (b != n) {
        q *= 2;
        b /= 2;
        print_intermediate(2, q, r);
        if (r >= b) {
            q += 1; 
            r -= b;
            print_intermediate(3, q, r);
        }
    }

    /* Return values */
    *rq = q;
    *rr = r;
}

/* 
 * Test the function by comparing the result 
 * to the expected; q = m / n and r = m % n
 */
bool test_func(int m, int n) {
    int q, r;
    func(m, n, &q, &r);
    if (q == m/n && r == m%n) 
        return true;
    else
        return false;
}


void main(void) {
    // We test more cases than we need to to execute all lines, just to make sure all types of input runs at they should (except n = 0, which results in a loop)
    assert(test_func(3, 17)); // Test m < n
    assert(test_func(28, 9)); // Test m > n
    assert(test_func(5, 5));  // Test m = n

    int m,n,q,r;
    scanf("%d%d", &m, &n);
    func(m, n, &q, &r);
    printf("q: %d, r: %d\n", q, r);
}