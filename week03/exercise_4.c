/* 
 * Week 3, exercise 4,
 * 
 * Write a program that computes the sum on the left hand side
 *  using two nested loops.
 * 
 *  n   m
 *  ∑   ∑ (2k − 1) = n(n+1)(2n+1) / 6
 * m=1 k=1
 * 
 * Provide test cases and test the program using the expression 
 * on the right hand sideof the equation.  Make sure all 
 * statements of your program are tested.
 * Hint: review the lecture slides Testability of a program
 * 
 */


/*
 * 1 input, n >= 1
 * 
 * sum = 0
 * for m from 1 to n
 *   for k from 1 to m
 *     sum = sum + (2k-1)
 * 
 */

#include <stdio.h>
#include <assert.h>
#include <stdbool.h>

int summing(int n) {
    assert(n >= 1);
    int sum, m, k; /* local variables */
    sum = 0;
    for (m = 1; m <= n; m++) {
        for (k = 1; k <= m; k++) {
            sum += 2*k - 1;
        }
    }
    return sum;
}

int alt_expression(int n) {
    return (n*(n+1)*(2*n+1)) / 6;
}

bool test_summing(void) {
    /* Test cases:
     * n = 1, 2, 5, 20
     */
    if (summing(1) != alt_expression(1))
        return false;
    else if (summing(2) != alt_expression(2))
        return false;
    else if (summing(5) != alt_expression(5))
        return false;
    else if (summing(20) != alt_expression(20))
        return false;
    else
        return true;
}

int main(void) {
    assert(test_summing());

    int n;
    scanf("%d", &n);
    int sum = summing(n);
    printf("%d\n", sum);
}