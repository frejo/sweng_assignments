
#include <assert.h>
#include "stack_lib.h"
#include "tree_lib.h"

int dfs(int x, tree* t) {
    /* Using the algorithm from the lecture:
        Push the root on to the stack, and do the following until the stack is empty
            • Pop the next node
            • <this is the currently visited node, do any data check or data manipulation here>
            • Push all children on to the stack
     */

    stack s;
    int i;
    tree_node* current;

    initialize_stack(&s);
    i = 0;

    push(t->root, &s);

    printf("\nSearching for %d...\n", x);

    while (stack_is_empty(&s) == false) {
        current = pop(&s);

        printf("%d ", current->value);

        if (current->value == x) {
            printf("\nFound %d at position %d\n", x, i);
            return i;
        }
        i++;

        if (current->right_child != NULL) {
            push(current->right_child, &s);
        }
        if (current->left_child != NULL) {
            push(current->left_child, &s);
        }
    }
    printf("\nDidn't find %d\n", x);
    return -1;
}

void test_dfs() {
    tree tt;

    tt.root = add_node(4,
                       add_node(7,
                                add_node(28,
                                         add_node(77, NULL, NULL),
                                         add_node(23, NULL, NULL)),
                                add_node(86,
                                         add_node(3, NULL, NULL),
                                         add_node(9, NULL, NULL))),
                       add_node(98, NULL, NULL));
    printf("Testing using the following tree:\n");
    print_tree(&tt);

    assert(dfs(100, &tt) == -1);

    assert(dfs(4, &tt) == 0);
    assert(dfs(7, &tt) == 1);
    assert(dfs(28, &tt) == 2);
    assert(dfs(77, &tt) == 3);
    assert(dfs(23, &tt) == 4);
    assert(dfs(86, &tt) == 5);
    assert(dfs(3, &tt) == 6);
    assert(dfs(9, &tt) == 7);
    assert(dfs(98, &tt) == 8);
}

int main() {
    test_dfs();
}