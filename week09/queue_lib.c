// From weeek 8

#include "queue_lib.h"

void initialize_queue(queue* q) {
    q->first_node = NULL;
    q->last_node = NULL;
    q->size = 0;
}

void enqueue(tree_node* x, queue* q) {
    /* Add an integer to the end of the queue */
    if (queue_is_full(q) == false) {
        node* new_node = malloc(sizeof(node));

        node* last = q->last_node;

        /* Set values of the new node */
        new_node->value = x;
        new_node->next_node = NULL;

        if (q->first_node == NULL) {
            /* When the stack is empty, the new node is be both the first and
             * last */
            q->first_node = new_node;
            q->last_node = new_node;
        } else {
            /* The alst node points the the new node, which is then made the
             * last node */
            last->next_node = new_node;
            q->last_node = new_node;
        }
        q->size++;
    }
}

tree_node* dequeue(queue* q) {
    /* Remove the item in the front of the queue and return it's integer value
     */
    tree_node* x;
    node* first;

    first = q->first_node;
    x = first->value;

    if (first != NULL) {
        q->first_node = first->next_node;
        free(first);
        if (q->first_node == NULL) {
            q->last_node = NULL;
        }

        q->size--;
    } else {
        printf("Queue underflow\n");
    }

    return x;
}

void empty_queue(queue* q) {
    /* Dequeue until the queue is empty */
    while (queue_is_empty(q) != true) {
        dequeue(q);
    }
}

bool queue_is_empty(queue* q) {
    /* Return whetyyher the queue is empty */
    return (q->first_node == NULL && q->last_node == NULL);
}

bool queue_is_full(queue* q) {
    /* check if the queue is less that 255 nodes long, since the variable
     * storing the size is 8 bit and cannot exceed 255 */
    return q->size == 255;
}

void print_queue(queue* q) {
    /* Print each item in the queue as either int or char as specified in the
     * node */
    node* current_node = q->first_node;
    while (current_node != NULL) {
        printf("%d ", current_node->value->value);

        current_node = current_node->next_node;
    }
    printf("\n");
}