#include "tree_lib.h"

tree_node* add_node(int x, tree_node* lchild, tree_node* rchild) {
    tree_node* new_node = malloc(sizeof(tree_node));
    new_node->value = x;
    new_node->left_child = lchild;
    new_node->right_child = rchild;
    return new_node;
}

void free_children(tree_node* n) {
    tree_node* lchild;
    tree_node* rchild;

    lchild = n->left_child;
    rchild = n->right_child;

    if (lchild != NULL) {
        free_children(lchild);
    }
    if (rchild != NULL) {
        free_children(rchild);
    }
}

void free_tree(tree* t) {
    free_children(t->root);
    free(t->root);
    t->root = NULL;
}

void print_node(tree_node* n, int level) {
    int x = n->value;
    tree_node* lchild = n->left_child;
    tree_node* rchild = n->right_child;

    if (lchild != NULL)
        print_node(lchild, level + 1);
    printf("%*c[%d]\n", level * 3 + 1, ' ', x);
    if (rchild != NULL)
        print_node(rchild, level + 1);
}

void print_tree(tree* t) {
    /*
     *  Tree:
     *  A:
     *    B
     *    C:
     *      D:
     *        F
     *        G
     *      E:
     *        H
     *        I
     */
    print_node(t->root, 0);
}
