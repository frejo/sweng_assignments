
#include <assert.h>
#include "queue_lib.h"
#include "tree_lib.h"

int bfs(int x, tree* t) {
    /* Using the algorithm from the lecture:
        Enqueue the root to the queue, and do the following until the queue is empty
            • Dequeue the next node
            • <this is the currently visited node, do any data check or data manipulation here>
            • Enqueue all children to the queue
     */

    queue q;
    int i;
    tree_node* current;

    initialize_queue(&q);
    i = 0;

    enqueue(t->root, &q);

    printf("\nSearching for %d...\n", x);

    while (queue_is_empty(&q) == false) {
        current = dequeue(&q);

        printf("%d ", current->value);

        if (current->value == x) {
            printf("\nFound %d at position %d\n", x, i);
            return i;
        }
        i++;

        if (current->left_child != NULL) {
            enqueue(current->left_child, &q);
        }
        if (current->right_child != NULL) {
            enqueue(current->right_child, &q);
        }
    }
    printf("\nDidn't find %d\n", x);
    return -1;
}

void test_bfs() {
    tree tt;

    tt.root = add_node(4,
                       add_node(7,
                                add_node(28,
                                         add_node(77, NULL, NULL),
                                         add_node(23, NULL, NULL)),
                                add_node(86,
                                         add_node(3, NULL, NULL),
                                         add_node(9, NULL, NULL))),
                       add_node(98, NULL, NULL));
    printf("Testing using the following tree:\n");
    print_tree(&tt);

    assert(bfs(100, &tt) == -1);

    assert(bfs(4, &tt) == 0);
    assert(bfs(7, &tt) == 1);
    assert(bfs(98, &tt) == 2);
    assert(bfs(28, &tt) == 3);
    assert(bfs(86, &tt) == 4);
    assert(bfs(77, &tt) == 5);
    assert(bfs(23, &tt) == 6);
    assert(bfs(3, &tt) == 7);
    assert(bfs(9, &tt) == 8);
}

int main() {
    test_bfs();
}