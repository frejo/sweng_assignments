
#ifndef _TREE_LIB_H
#define _TREE_LIB_H

#include <stdlib.h>
#include <stdio.h>

typedef struct tree_node {
    int value;
    struct tree_node* left_child;
    struct tree_node* right_child;
} tree_node;

typedef struct tree {
    tree_node* root;
} tree;

tree_node* add_node(int x, tree_node* lchild, tree_node* rchild);
void free_node(tree_node* n);
void free_tree(tree* t);
void print_node(tree_node* n, int level);
void print_tree(tree* t);

#endif //_TREE_LIB_H