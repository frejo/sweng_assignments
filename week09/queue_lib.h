// Based on my queue lib from previous weeks, but storing tree_node pointers
// instead of ints.

/* Library for a queue of mixed ints and chars */

#ifndef _queue_lib_h
#define _queue_lib_h

#include <assert.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include "tree_lib.h"

typedef struct node {
    /* A node holds a type and either a char or int as value and a pointer the
     * next node in the queue */
    tree_node* value;
    struct node* next_node;
} node;

typedef struct queue {
    node* first_node;
    node* last_node;
    int size;
} queue;

void initialize_queue(queue* q);

void enqueue(tree_node* x, queue* q);
tree_node* dequeue(queue* q);

void empty_queue(queue* q);
bool queue_is_empty(queue* q);
bool queue_is_full(queue* q);
void print_queue(queue* q);

#endif
