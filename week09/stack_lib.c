// Based on my stack lib from previous weeks, but storing tree_node pointers instead of ints.

#include "stack_lib.h"

void initialize_stack(stack* s) {
    /* Set the stack to NULL pointer */
    s->top_node = NULL;
}

void push(tree_node* x, stack* s) {
    /* Push the given value to the top of the stack */

    /* make a new node with given value */
    node* this_node = malloc(sizeof(node));
    this_node->data = x;
    this_node->lower_node = s->top_node; /* The new node should point to the last node, which is currently the top of the stack */
    s->top_node = this_node;             /* Make the new note top */
}

tree_node* pop(stack* s) {
    /* Remove the top item in the stack and return its value */

    /* make a pointer to the top node, and store its value in a variable */
    node* this_node = s->top_node;
    tree_node* this_value = this_node->data;

    /* If the stack isn't empty, set the top node to the second top node, and free the memory assigned to the top node */
    node* last_node = this_node->lower_node;
    if (stack_is_empty(s) == false) {
        s->top_node = last_node;
        free(this_node);
    } else {
        /* We cannot pop an empty stack */
        printf("Stack underflow\n");
    }

    return this_value;
}

bool stack_is_empty(stack* s) {
    /* Check whether the stack is empty */
    return s->top_node == NULL;
}

void print_stack(stack s) {
    /* Prints stack with the top first */

    node* current_node = s.top_node;
    if (current_node == NULL) {
        printf("Stack empty\n");
        return;
    }
    printf("Stack: ");
    while (current_node != NULL) {
        int value = current_node->data->value;
        printf("%d ", value);
        current_node = current_node->lower_node;
    }
    printf("\n");
}
