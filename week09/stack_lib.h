// Based on my stack lib from previous weeks, but storing tree_node pointers instead of ints.


#ifndef _STACK_LIB_H
#define _STACK_LIB_H

#include <stdbool.h>    
#include <stdlib.h>
#include <stdio.h>
#include "tree_lib.h"

typedef struct node {
    tree_node* data;
    struct node* lower_node;
} node;

typedef struct stack {
    node* top_node;
} stack;

void initialize_stack(stack* s);
void push(tree_node* n, stack* s);
tree_node* pop(stack* s);
bool stack_is_empty(stack* s);
void print_stack(stack s);

#endif // ndef _STACK_LIB_H