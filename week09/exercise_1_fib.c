/*
 * Fibonacci numbers defined recursively
 */

#include <assert.h> /* assert */
#include <stdio.h>  /* printf */

#define A_INIT 0
#define B_INIT 1

/* Fibonacci function definition */
int fib(int n, int a, int b) {
    /* pre-condition */
    assert(n >= 0);

    /* post-condition */

    /* Original funcition from the lecture
    if(n > 1) 
        return fib(n - 1) + fib(n - 2);
    else if(n == 1)
        return 1;
    else
        return 0;
    */

    /* New function */
    /* For each recursion n gets smaller, 
      and when n hits 0, the i'th fibonacci 
      number has been found */

    if (n == 0) {
        return a;
    } else {
        return fib(n - 1, b, a + b);
    }
}

void test_fib() {
    assert(fib(0, A_INIT, B_INIT) == 0);
    assert(fib(1, A_INIT, B_INIT) == 1);
    assert(fib(2, A_INIT, B_INIT) == 1);
    assert(fib(3, A_INIT, B_INIT) == 2);
    assert(fib(7, A_INIT, B_INIT) == 13);
    assert(fib(40, A_INIT, B_INIT) == 102334155);
}

int main(void) {
    test_fib();

    int n;
    int f;

    printf("Find Fibonacci number n: ");
    scanf("%d", &n);

    f = fib(n, A_INIT, B_INIT);

    printf("%d\n", f);

    return 0;
}
