/* 
 * (PC-2.8.1) A sequence of n > 0 integers is called a 
 * jolly jumper if the absolute values of the 
 * differences between successive elements take on all 
 * possible values 1 through n−1. For instance, 1 4 2 3
 * is a jolly jumper, because the absolute differences 
 * are 3, 2, and 1, respectively. As another example, 
 * 11 7 4 2 1 6 is a jolly jumper, because the absolute 
 * differences are 4,3,2,1,5 (the order of the 
 * differences does not matter). The definition implies
 * that any sequence of a single integer is a jolly 
 * jumper. Write a program to determine whether each of 
 * a number of sequences is a jolly jumper. 
 * Hint: use a boolean array, e.g.bool diffsfound[n] to 
 * keep track of the differences found so far between
 * consecutive numbers. So that diffsfound[2]
 * being true implies that the absolute difference 2 has
 * already been found. 
 * Input 
 *     A line of input contains an integer n < 100 
 *     followed by n integers representing the sequence. 
 * Output 
 *     For the line of input generate a line of output 
 *     saying “Jolly” or “Not jolly”. 
 */

#include <stdio.h>
#include <assert.h>
#include <stdbool.h>

#define MAX_LENGTH 100

int max(int a, int b) {
    return a > b ? a : b;
}
int min(int a, int b) {
    return a < b ? a : b;
}

int get_n(void) {
    int n;
    scanf("%d", &n);
    return n;
}

void get_array(int* a, int len) {
    assert(len>0 && len < MAX_LENGTH);
    for (int i = 0; i < len; i++) {
        scanf("%d", (a+i));
    }
}

bool is_Jolly(int* a, int len) {
    bool diffs_found[len]; // Store a bool about whether each difference has been seen before.
    for (int i = 0; i < len; i++) {
        /* Reset the array */
        diffs_found[i] = false;
    }
    for (int i = 1; i < len; i++) {
        /* The difference is the highest of a_i and a_(i-1) minus the lowest of the two */
        int diff = max( *(a+i), *(a+i-1) ) - min( *(a+i), *(a+i-1) );
        printf("i: %d, diff: %d \n", i, diff);
        /* The sequence is not jolly when there has 
        been the same difference before, or if the 
        difference is bigger than the amount of 
        numbers in the array */
        if (diffs_found[diff-1] || diff >= len) {
            return false;
        } else {
            diffs_found[diff-1] = true;
        }
    }
    return true;
}

int main(void) {
    printf("Enter an integer n followed by n integers: \n> ");
    int n = get_n();
    int a[n];
    get_array(a, n);
    if (is_Jolly(a, n)) {
        printf("Your array is jolly 😃\n");
    } else {
        printf("Your array is not jolly 😞\n");
    }
}
