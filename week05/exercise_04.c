/*
 * In the lecture we discussed how to represent a 
 *  geometric point using a C struct. Let’s now 
 *  consider a geometric circle:  a circle consists 
 *  of three integers: x coordinate of the centre 
 *  point, y coordinate of the centre point, and a 
 *  radius. 
 * 
 * (a)  Write  a  C  program  that  represents  a 
 *      circle using  a  C  struct  with  three  
 *      integers. Test  your  struct by creating 
 *      circles c1, c2, c3 with  different  values  
 *      for (x, y, radius).
 * (b)  Create an array of five circles C[5] such 
 *      that circle ci has centre point (i, i) and 
 *      radius i
 * (c)  Create  a  function circleIsValid that takes 
 *      a  pointer  to  a  circle  as  input,  and 
 *      returns true if the radius of the circle is 
 *      positive (r > 0) and false otherwise. Use 
 *      assertions to test your function by passing 
 *      in some valid and invalid circles. 
 *      Hint: it’s a good idea to draw, using pen 
 *      and paper, an example of a circle struct 
 *      variable, and a pointer to this variable, 
 *      like in the lectures.
 * (d)  Create a function translate that takes a 
 *      pointer to a circle c and a pointer to a 
 *      geometric point p (like in the lecture), 
 *      and adds the coordinate values of p to the 
 *      centre point coordinate values of c, i.e.  it 
 *      translates the circle by a vector represented 
 *      by the point p.  For example, if c is initially 
 *      centred at (5,10) and we pass, as input, a 
 *      point p with coordinate values (1,−1) then the 
 *      centre point of c becomes (6,9) after the 
 *      translate function. Demonstrate your function 
 *      with a few test calls and use assertions to 
 *      validate your code.
 */

#include <stdio.h>
#include <assert.h>
#include <stdbool.h>

typedef struct {
    int x;
    int y;
    int r;
} circle;

typedef struct {
    int x;
    int y;
} point;

bool circleIsValid(circle* C) {
    /* Return whether the radius is bigger than 0 or not */
    return C->r > 0 ? true : false; 
}

void printCircle(circle* C) {
    printf("(x=%d, y=%d, r=%d)", C->x, C->y, C->r);
}

void test_circleIsValid(void) {
    /* Make the circle with the center point (0,0) */
    circle C = {.x = 0, .y = 0};
    /* Assert that the circle is invalid with radii from -5 through 0 and valid from 1 through 5 */
    for (int i = -5; i <= 5; i++) {
        C.r = i;
        printf("   Is the circle ");
        printCircle(&C);
        printf(" valid? ");
        printf(circleIsValid(&C) ? "true" : "false");
        printf(". Expected ");
        if (i <= 0) {
            printf("false\n");
            assert(!circleIsValid(&C));
        } else {
            printf("true\n");
            assert(circleIsValid(&C));
        }
    }
}

void translate(circle* C, point* P) {
    printf("   Translating circle (%d, %d, %d) with vector (%d, %d).\n", C->x, C->y, C->r, P->x, P->y);
    C->x += P->x;
    C->y += P->y;
    printf("   New circle: (%d, %d, %d).\n", C->x, C->y, C->r);
} 

void test_translate(void) {
    /* Test translating a circle a little around */
    circle C = {.x = 5, .y = 10, .r = 1};
    point P = {.x = 1, .y = -1};
    translate(&C, &P);
    assert(C.x == 6 && C.y == 9);

    P.x = 3; 
    P.y = -3;
    translate(&C, &P);
    assert(C.x == 9 && C.y == 6);

    P.x = -2; 
    P.y = 1;
    translate(&C, &P);
    assert(C.x == 7 && C.y == 7);

    P.x = -14; 
    P.y = -16;
    translate(&C, &P);
    assert(C.x == -7 && C.y == -9);
}

int main(void) {
    circle c1 = {.x = 1, .y = 2, .r = 3};
    circle c2 = {.x = 3, .y = 2, .r = 1};
    circle c3 = {.x = 2, .y = 4, .r = 6};
    printf("a) Test of struct circle: \n   c1: ");
    printCircle(&c1);
    printf("\n   c2: ");
    printCircle(&c2);
    printf("\n   c3: ");
    printCircle(&c3);
    printf(".\n");

    /* Array C[5] where circle C[i] has the center point (i,i) and a radius i. This means that circle C[0] will be invalid */
    circle C[5];
    for (int i = 0; i < 5; i++) {
        C[i].x = i;
        C[i].y = i;
        C[i].r = i;
    }
    printf("b) The five circles in C[5]: \n");
    for (int i = 0; i < 5; i++) {
        printf("   C[%d] : ", i);
        printCircle(&C[i]);
        printf(" is valid: ");
        printf(circleIsValid(&C[i]) ? "true" : "false");
        printf(".\n");
    }

    printf("c) Testing function circleIsValid with circles with the radii from -5 through 5:\n");
    test_circleIsValid();
    printf("d) Testing translate function:\n");
    test_translate();
}

