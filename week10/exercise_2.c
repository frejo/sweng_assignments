#include <assert.h>
#include <stdbool.h>
#include <stdio.h>
#include "llist_lib.h"

int sum_squares(node* l) {
    int x, r;
    node* t;

    x = l->val;
    t = l->next_node;

    /* Pre-condition */
    assert(l != NULL);

    /* Post-condition */
    r = x * x;

    if (t != NULL) {
        r += sum_squares(t);
    }
    return r;
}

bool test_sum_squares(void) {
    int sum;
    node* list =
        make_node(1,
                  make_node(2,
                            make_node(3,
                                      make_node(4,
                                                make_node(5, NULL)))));
    sum = sum_squares(list);
    free_list(list);

    if (sum != 55)
        return false;

    list =
        make_node(1,
                  make_node(-2,
                            make_node(-3,
                                      make_node(-4,
                                                make_node(5, NULL)))));
    sum = sum_squares(list);
    free_list(list);

    if (sum != 55)
        return false;

    return true;
}

int main(void) {
    assert(test_sum_squares());

    return 0;
}
