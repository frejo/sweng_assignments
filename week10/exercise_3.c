#include <assert.h>
#include <stdbool.h>
#include <stdio.h>
#include "llist_lib.h"

typedef int (*func)(int x);

int square(int x) {
    return x * x;
}

node* map(node* l, func f) {
    node* new_list;
    node* tail = NULL;

    assert(l != NULL);

    if (l->next_node != NULL) {
        tail = map(l->next_node, f);
    }
    new_list = make_node(f(l->val), tail);
    return new_list;
}

bool IsEqual(node* l1, node* l2) {
    if (l1->next_node == NULL && l2->next_node == NULL) {
        return (l1->val == l2->val);
    } else if ((l1->next_node == NULL) != (l2->next_node == NULL)) {
        /* The lists have different length */
        return false;
    } else {
        return IsEqual(l1->next_node, l2->next_node);
    }
}

int main(void) {
    node* list =
        make_node(1,
                  make_node(2,
                            make_node(3,
                                      make_node(4,
                                                make_node(5, NULL)))));
    node* list_compare =
        make_node(1,
                  make_node(4,
                            make_node(9,
                                      make_node(16,
                                                make_node(25, NULL)))));
    node* list2 = map(list, square);
    print_llist(list);
    printf("\n");
    print_llist(list2);
    printf("\n");

    assert(IsEqual(list2, list_compare));

    free_list(list);
    free_list(list2);
    free_list(list_compare);

    return 0;
}
