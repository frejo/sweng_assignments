#include <assert.h>
#include <stdio.h>
#include "llist_lib.h"

int main(void) {
    node* list = make_node(1,
                           make_node(2,
                                     make_node(3,
                                               make_node(4, NULL))));
    
    const char* strlst = list_to_str(list);
    
    /* Test the list has been correctly turned into a string */
    assert( list_str_len(list) == 8 ); // 4 single digits, 3 spaces, 1 NULL terminator
    assert( strcmp(strlst, "1 2 3 4") == 0 ); // strcmp returns 0 when strings are equal

    /* Print list */
    print_llist(list);

    free((char*) strlst);
    free_list(list);

    return 0;
}
