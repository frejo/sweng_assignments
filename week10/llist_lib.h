#ifndef _LINKED_LIST_LIB_H
#define _LINKED_LIST_LIB_H

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h> 

typedef struct node
{
    int val;
    struct node* next_node;
} node;

node* make_node(int i, node* p);
void free_list(node* head);
void print_llist(node* head);
int list_str_len(node* list);
const char* list_to_str(node* head);

#endif //_LINKED_LIST_LIB_H