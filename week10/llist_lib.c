#include "llist_lib.h"

node* make_node(int i, node* p) {
    node* q = malloc(sizeof(node));
    q->val = i;
    q->next_node = p;
    return q;
}

void free_list(node* head) {
    /* Free the tail if it's not empty then free the head */
    node* tail = head->next_node;

    /* Pre-condition */
    assert(head != NULL);

    /* Post-condition */
    if (tail != NULL) {
        free_list(tail);
    }
    free(head);
}

int list_str_len(node* list) {
    /* Return the neccessary length of a string able to display the list */
    int counter = 0;
    node* next = list;
    while (next != NULL) {
        counter += snprintf(NULL, 0, "%d", next->val) + 1; // add 1 for space or NULL terminator
        next = next->next_node;
    }
    return counter;
}

const char* list_to_str(node* head) {
    /* Turn the list into a string (Needed to test the print function)*/
    node* tail = head->next_node;
    int val;
    char* str = calloc(list_str_len(head), sizeof(char)); // Get the neccessary length and allocate space for the string
     
    /* Pre-condition */
    assert(head != NULL);

    val = head->val;
    int length = snprintf(NULL, 0, "%d", val); /* This returns the count of chars needed to display the int val */
    char* num = calloc(length+1, sizeof(char)); /* Allocate space for this size +1 for the NULL terminator */
    sprintf(num, "%d", val);    /* Print val to the num string */

    strcat(str, num);   /* Combine the two strings */
    if (tail != NULL) {
        /* If there's any more numbers in the list */
        const char* tail_str = list_to_str(tail); /* Get the tail as a string */
        strcat(str, " ");         /* Combine the current string and the tail string with a space seperator */
        strcat(str, tail_str);  
        free((char*) tail_str); /* Free memory of the tail string. Cast as (char*) instead of (const char*) to get rid of compile warning */
    }
    free(num);
    return str;
}

void print_llist(node* head) {
    // /* Print the head, then print the tail */
    // node* tail = head->next_node;
    
    // /* Pre-condition */
    // assert(head != NULL);

    // /* Post-condition */
    // printf("%d ", head->val);
    // if (tail != NULL) {
    //     print_llist(tail);
    // }

    const char* strlst = list_to_str(head);

    printf( "%s\n", strlst );

    free((char*) strlst);
}