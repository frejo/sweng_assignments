#include <assert.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

typedef struct node {
    int value;
    struct node* lchild;
    struct node* rchild;
} node;

typedef struct tree {
    struct node* root;
} tree;

void Initialize(tree* t) {
    t->root = NULL;
}

bool Contains(int x, tree* t);
node* MakeNode(int x, node* left, node* right);
void Insert(int x, tree* t);
void MoveTreeToTree(tree* src, tree* dst);
void Remove(int x, tree* t);
bool IsFull(tree* t);
bool IsEmpty(tree* t);
void PrintNode(node* n, int level);
void PrintTree(tree* t);


bool Contains(int x, tree* t) {
    node* root = t->root;
    tree left = {root->lchild};
    tree right = {root->rchild};

    if (root == NULL) {
        return false;
    } else if (x == root->value) {
        return true;
    } else if (x < root->value) {
        return Contains(x, &left);
    } else /* x > root->value */ {
        return Contains(x, &right);
    }
}

node* MakeNode(int x, node* left, node* right) {
    node* n = malloc(sizeof(node));
    n->value = x;
    n->lchild = left;
    n->rchild = right;
    return n;
}

void Insert(int x, tree* t) {
    node* root = t->root;

    if (IsEmpty(t)) {
        t->root = MakeNode(x, NULL, NULL);
    } else {
        tree left = {root->lchild};
        tree right = {root->rchild};
        if (x <= root->value) {
            Insert(x, &left);
        } else /* x > root->value */{
            Insert(x, &right);
        }
    }
}

void MoveTreeToTree(tree* src, tree* dst) {
    if (src == NULL)
        return;
    
    node* src_root = src->root;
    tree src_left = {src_root->lchild};
    tree src_right = {src_root->rchild};

    bool leftHasChildren = (IsEmpty(&src_left) == false);
    bool rightHasChildren = (IsEmpty(&src_right) == false);

    if (leftHasChildren)
        MoveTreeToTree(&src_left, dst);
    if (rightHasChildren)
        MoveTreeToTree(&src_right, dst);
    
    Insert(src_root->value, dst);
    free(src_root);
    return;
}

void Remove(int x, tree* t) {
    node* root = t->root;
    tree left = {root->lchild};
    tree right = {root->rchild};

    if (Contains(x, t) == false) {
        return;
    }

    if (x == root->value) {
        if (Contains(x, &left)) {
            Remove(x, &left);
            return;
        } else {
            MoveTreeToTree(&right, &left);

            free(t->root);
            t->root = left.root;
        }
    }
}

bool IsFull(tree* t) {
    return false;
}

bool IsEmpty(tree* t) {
    return (t->root == NULL);
}

void PrintNode(node* n, int level) {
    if (n == NULL) return;
    int x = n->value;
    tree left = {n->lchild};
    tree right = {n->rchild};

    if (IsEmpty(&left) != true) {
        PrintNode(left.root, level + 1);
    }
    printf("%*c[%d]\n", level * 3 + 1, ' ', x);
    if (IsEmpty(&right) != true) {
        PrintNode(right.root, level + 1);
    }
}

void PrintTree(tree* t) {
    PrintNode(t->root, 0);
}


void main(void) {
    tree test;
    Initialize(&test);
    Insert(2, &test);
    Insert(5, &test);
    Insert(1, &test);
    Insert(3, &test);
    Insert(2, &test);
    Insert(6, &test);
    Insert(8, &test);
    Insert(7, &test);
    Insert(8, &test);
    Insert(8, &test);
    PrintTree(&test);
}
