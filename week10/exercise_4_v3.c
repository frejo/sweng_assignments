#include <assert.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#define MAX_TREE_SIZE 524288  // 512KiB

typedef struct node {
    bool isRoot;  // Is set true for root of tree. The root only uses one child (left)
    int value;
    struct node* lchild;
    struct node* rchild;
    unsigned int children;
} node;

const int Max_Children = MAX_TREE_SIZE / sizeof(node) - 1; /* Calculate maximum number of children */

void Insert(int x, node* t);
void Remove(int x, node* t);
bool Contains(int x, node* t);
bool IsFull(node* t);
bool IsEmpty(node* t);
node* MakeNode(int x, node* left, node* right);
void PrintNode(node* t, int level);

node* MakeNode(int x, node* left, node* right) {
    node* t = malloc(sizeof(node));
    t->value = x;
    t->isRoot = false;
    t->lchild = left;
    t->rchild = right;
    t->children = 0;
}

bool IsFull(node* t) {
    return (t->children >= Max_Children);
}

bool IsEmpty(node* t) {
    if (t == NULL)
        return true;

    if (t->isRoot)
        return IsEmpty(t->lchild);

    return false;
}

void Initialize(node* t) {
    /* Set the node to be the root */
    t->isRoot = true;
    t->lchild = NULL;
    t->rchild = NULL;
    t->children = 0;
}

bool Contains(int x, node* t) {
    /* Returns true if x is found in the tree */
    if (IsEmpty(t))
        return false;

    if (t->isRoot)
        return Contains(x, t->lchild);

    if (x == t->value) {
        return true;
    } else if (x < t->value) {
        return Contains(x, t->lchild);
    } else /* x > t->value */ {
        return Contains(x, t->rchild);
    }
}

void Insert(int x, node* t) {
    if (IsFull(t)) /* Don't insert in a full tree */
        return;

    t->children++;

    if (IsEmpty(t)) {
        if (t->isRoot) {
            t->lchild = MakeNode(x, NULL, NULL);
        } else {
            /* This shouldn't happen */
            printf("ERROR: Cannot insert in non-root empty tree.\n");
            abort();
        }
    } else if (t->isRoot) {
        /* Recursively insert up the tree */
        Insert(x, t->lchild);
    } else if (x <= t->value) {
        if (IsEmpty(t->lchild))
            t->lchild = MakeNode(x, NULL, NULL);
        else
            Insert(x, t->lchild);
    } else /* x > t->value */ {
        if (IsEmpty(t->rchild))
            t->rchild = MakeNode(x, NULL, NULL);
        else
            Insert(x, t->rchild);
    }
}

void Remove(int x, node* t) {
    if (Contains(x, t) == false)
        return;

    t->children--;

    if (t->isRoot) {
        Remove(x, t->lchild);
        return;
    }

    /* Remove x from a higher position in the tree if there is one */
    if (Contains(x, t->lchild)) {
        Remove(x, t->lchild);
    } else if (Contains(x, t->rchild)) {
        Remove(x, t->rchild);
    } else if (x == t->value) {
        /* 
            To remove the current node:
            - Combine the children into one tree
            - Set pointers from the parent to the new tree
            - Remove the node
            
            To be able to set the pointer from the parent to the new child, 
            we have to mark the current node for removal. Then it can be 
            removed/freed by the parent after it has made the right pointer.
        */
        if (t->lchild == NULL) {
            /* The following code expects the new tree to be the left child. 
                If the left child is empty, make the left child be the right child */
            t->lchild = t->rchild;
            t->rchild = NULL;
        } else /* When there's something in the left child */ {
            /* Combine the left and the right children by moving the right child to the right-most leaf of the left child. */
            int nodes_in_right = t->rchild->children + 1;
            node* biggest_in_left = t->lchild;
            while (biggest_in_left->rchild != NULL) {
                biggest_in_left->children += nodes_in_right;
                biggest_in_left = biggest_in_left->rchild;
            }
            biggest_in_left->children += nodes_in_right;
            biggest_in_left->rchild = t->rchild;
        }
        /* Tag node for deletion by making it a root. */
        t->value = 0;
        t->rchild = NULL;
        t->isRoot = true;
    } else {
        /* Shouldn't be possible */
        printf("ERROR: x (%d) is in tree, but not found in left child, right child or the node itself. \n", x);
        abort();
    }

    /* If any node is parent to a node marked root, 
    the parent should point to the children's left 
    child storing the combined tree, and the child 
    should be freed */

    /* Remove and free root nodes inside the tree (children that has been tagged for removal) */
    if (t->lchild != NULL && t->lchild->isRoot == true) {
        /* A left child is marked root */
        node* middle = t->lchild;
        t->lchild = t->lchild->lchild;
        free(middle);
    }
    if (t->rchild != NULL && t->rchild->isRoot == true) {
        /* A right child is marked root */
        node* middle = t->rchild;
        t->rchild = t->rchild->lchild;
        free(middle);
    }
}

void PrintNode(node* t, int level) {
    /* Print the tree from left branching to the right */
    if (IsEmpty(t))
        return;

    if (t->isRoot) {
        PrintNode(t->lchild, level);
    } else {
        int x = t->value;
        node* left = t->lchild;
        node* right = t->rchild;

        /* Print each node with 3 space indentation per level */
        if (IsEmpty(left) != true)
            PrintNode(left, level + 1);
        printf("Children: %d\t%*c[%d]\n", t->children, level * 3 + 1, ' ', x);
        if (IsEmpty(right) != true)
            PrintNode(right, level + 1);
    }
}

bool test_tree(void) {
    node tree_node;
    node* tree = &tree_node;
    int x, y, z;

    Initialize(tree);

    /* Rule A - Empty after initialization */
    if (IsEmpty(tree) == false) {
        printf("Error rule A\n");
        return false;
    }

    /* Rule B - Insert(x,t) immediately followed by Remove(x,t), must result in the same tree as before */
    x = 4;
    Insert(x, tree);
    Remove(x, tree);
    if (IsEmpty(tree) == false) {
        printf("Error rule B\n");
        return false;
    }

    /* Rule C - After inserting x, Contains(x,t) should be true */
    Insert(x, tree);
    if (Contains(x, tree) == false) {
        printf("Error rule C\n");
        return false;
    }

    /* Rule D - After multiple inserts and then removing the root, the tree must still contain the children of the removed root */
    y = 9;
    // Insert(x, tree); // isn't removed after test of Rule C
    Insert(y, tree);
    Remove(x, tree);
    if (Contains(y, tree) == false) {
        printf("Error rule D\n");
        return false;
    }

    Remove(y, tree);  // Empty the tree

    /* Rule E - Duplicates should work; After inserting x twice, and removing it once, it should still contain x. After another remove, the tree should not contain x. */
    Insert(x, tree);
    Insert(x, tree);
    Remove(x, tree);
    if (Contains(x, tree) == false) {
        printf("Error rule E.1\n");
        return false;
    }
    Remove(x, tree);
    if (Contains(x, tree) == true) {
        printf("Error rule E.2\n");
        return false;
    }
    return true;
}

int main(void) {
    assert(test_tree());
    node tt;
    node* tp = &tt;

    Initialize(tp);

    /* Visual test. Insert all items in inarr and remove the items in outarr and print the tree after each operation */
    int inarr[] = {2, 1, 3, 2, -5, -6, -2, 4, 8, 6, 5};
    int outarr[] = {2, 2, 5, 1};
    for (int i = 0; i < sizeof(inarr) / sizeof(inarr[0]); i++) {
        printf("~~~ Insert %d ~~~\n", inarr[i]);
        Insert(inarr[i], tp);
        PrintNode(tp, 0);
    }
    for (int i = 0; i < sizeof(outarr) / sizeof(outarr[0]); i++) {
        printf("~~~ Remove %d ~~~\n", outarr[i]);
        Remove(outarr[i], tp);
        PrintNode(tp, 0);
    }

    return 0;
}