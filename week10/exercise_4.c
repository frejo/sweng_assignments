#include <assert.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

typedef struct node {
    int item;
    struct node* lchild;
    struct node* rchild;
} node;

node* Initialize(node* t);
node* Insert(int x, node* t);
node* Remove(int x, node* t);
bool Contains(int x, node* t);
bool Is_Full(node* t);
bool Is_Empty(node* t);
void PrintTree(node* t, int level);


node* Initialize(node* t) {
    return NULL;
}

node* MakeNode(int x, node* left, node* right) {
    node* n = malloc(sizeof(node));
    n->item = x;
    n->lchild = left;
    n->rchild = right;
    return n;
}

node* Insert(int x, node* t) {
    if (Is_Empty(t)) {
        t = MakeNode(x, NULL, NULL);
    } else if (x <= t->item) {
        t->lchild = Insert(x, t->lchild);
    } else {
        t->rchild = Insert(x, t->rchild);
    }

    return t;
}

node* MoveTreeToTree(node* t_source, node* t_dest) {
    node* combined_tree;

    if (t_source == NULL)
        return t_dest;

    bool leftHasChildren = (Is_Empty(t_source->lchild) == false);
    bool rightHasChildren = (Is_Empty(t_source->rchild) == false);

    if (leftHasChildren) {
        combined_tree = MoveTreeToTree(t_source->lchild, t_dest);
    }

    if (rightHasChildren) {
        combined_tree = MoveTreeToTree(t_source->rchild, t_dest);
    }

    combined_tree = Insert(t_source->item, t_dest);
    free(t_source);

    return combined_tree;
}

node* Remove(int x, node* t) {
    if (Contains(x, t) == false) {
        return t;
    }

    if (x == t->item) {
        /* Remove node from left child in case of duplicates */
        if (Contains(x, t->lchild)) {
            t->lchild = Remove(x, t->lchild);
        } else {
            node* new_tree = MoveTreeToTree(t->rchild, t->lchild);
            free(t);
            return new_tree;
        }
    } else if (x < t->item) {
        t->lchild = Remove(x, t->lchild);
    } else /* (x > t->item) */ {
        t->rchild = Remove(x, t->rchild);
    }
    return t;
}

bool Contains(int x, node* t) {
    if (t == NULL) {
        return false;
    } else if (x == t->item) {
        return true;
    } else if (x < t->item) {
        return Contains(x, t->lchild);
    } else {
        return Contains(x, t->rchild);
    }
}

bool Is_Full(node* t) {
    return false;
}

bool Is_Empty(node* t) {
    return (t == NULL);
}

void PrintTree(node* t, int level) {
    int x = t->item;
    node* left = t->lchild;
    node* right = t->rchild;

    if (Is_Empty(left) != true) {
        PrintTree(left, level + 1);
    }
    printf("%*c[%d]\n", level * 3 + 1, ' ', x);
    if (Is_Empty(right) != true) {
        PrintTree(right, level + 1);
    }
}

bool test_tree(void) {
    node* tree;
    int x, y, z;

    tree = Initialize(tree);

    /* Rule A - Empty after initialization */
    if (Is_Empty(tree) == false) {
        printf("Error rule A\n");
        return false;
    }
    
    /* Rule B - Insert(x,t) immidiatly followed by Remove(x,t), must result in the same tree as before */
    x = 4;
    tree = Insert(x, tree);
    tree = Remove(x, tree);
    if (Is_Empty(tree) == false) {
        printf("Error rule B\n");
        return false;
    }

    /* Rule C - After inserting x, Contains(x,t) should be true */
    tree = Insert(x, tree);
    if (Contains(x, tree) == false) {
        printf("Error rule C\n");
        return false;
    }

    /* Rule D - After multiple inserts and then removing the root, the tree must still contain the children of the removed root */
    y = 9;
    tree = Insert(y, tree);
    tree = Remove(x, tree);
    if (Contains(y, tree) == false) {
        printf("Error rule D\n");
        return false;
    }

    tree = Remove(y, tree); // Empty the tree
    
    /* Rule E - Duplicates should work; After inserting x twice, and removing it once, it should still contain x. After another remove, the tree should not contain x. */
    tree = Insert(x, tree);
    tree = Insert(x, tree);
    tree = Remove(x, tree);
    if (Contains(x, tree) == false) {
        printf("Error rule E.1\n");
        return false;
    }
    tree = Remove(x, tree);
    if (Contains(x, tree) == true) {
        printf("Error rule E.2\n");
        return false;
    }
    return true;
}

void DeleteTree(node* t) {
    while (Is_Empty(t) != true)
    {
        t = Remove(t->item, t);
    }
}

void ManualTest(void) {
    node* tree;
    tree = NULL;
    tree = Insert(2, tree);
    tree = Insert(5, tree);
    tree = Insert(1, tree);
    tree = Insert(3, tree);
    tree = Insert(2, tree);
    tree = Insert(6, tree);
    tree = Insert(8, tree);
    tree = Insert(7, tree);
    tree = Insert(8, tree);
    tree = Insert(8, tree);
    PrintTree(tree, 0);
    printf("Contains 2? %d\n", Contains(2, tree));
    printf("Contains 3? %d\n", Contains(3, tree));
    printf("Contains 4? %d\n", Contains(4, tree));
    printf("Contains 7? %d\n", Contains(7, tree));
    printf("Remove 8\n");
    tree = Remove(8, tree);
    PrintTree(tree, 0);
    printf("Remove 1\n");
    tree = Remove(1, tree);
    PrintTree(tree, 0);
    printf("Remove 5\n");
    tree = Remove(5, tree);
    PrintTree(tree, 0);

    printf("Delete\n");
    DeleteTree(tree);
    PrintTree(tree, 0);
}

int main(void) {
    assert(test_tree());
    ManualTest();
    return 0;
}