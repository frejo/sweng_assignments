/*
 * Week 2, exercise 6:
 * 
 * Challenge: (PC-7.6.7) 
 * I collect marbles (colourful small glass balls) and want to buy boxes to 
 *  store them. The boxes come in two types,
 * Type 1: each such box costs c1 øre and can hold exactly n1 marbles
 * Type 2: each such box costs c2 øre and can hold exactly n2 marbles
 * I want each box to be filled to its capacity, and also to minimise 
 *  the total cost of buying them. Help me find the best way to distribute 
 *  my marbles among the boxes.
 * 
 * Input 
 *      The input begins with a line containing the integer 
 *      n(1 ≤ n ≤ 2,147,395,600). The second line contains c1 and n1, and the 
 *      third line contains c2 and n2. Here, c1, c2, n1, and n2 are all 
 *      positive integers having values smaller than 46,341.
 * Output 
 *      Print a line containing the minimum cost solution (two nonnegative 
 *       integers m1 and m2, where mi = number of type i boxes required if one 
 *       exists. Otherwise print "failed".
 * If a solution exists, you may assume that it is unique.
 * 
 * Author: Tobias Frejo Rasmussen
 */

#include <stdio.h>

int n, c1, c2, n1, n2, m1, m2; /* Input/Output values */
double value_1, value_2; /* c/n for both types */
int c_cheap, n_cheap, c_expensive, n_expensive; /* For storing which of type 1 and 2 is cheapest */
int i; /* Loop iterative */

int main(void) {
    /* Get input */
    printf("Enter number of marbles: ");
    scanf("%d", &n);
    printf("Enter price (in øre) and capacity of box type 1: ");
    scanf("%d%d", &c1, &n1);
    printf("Enter price (in øre) and capacity of box type 2: ");
    scanf("%d%d", &c2, &n2);

    /* Precondition */
    /* n(1 ≤ n ≤ 2,147,395,600) 
     * c_1, n_1, c_2, n_2 >=0 && < 46341
     */
    if (n < 1 ||
        n > 2147395600 ||  
        c1 < 0 || 
        c2 < 0 || 
        n1 < 0 || 
        n2 < 0 ||
        c1 >= 46341 || 
        c2 >= 46341 || 
        n1 >= 46341 || 
        n2 >= 46341
        ) 
    {
        printf("Bad input\n");
        return 1;
    }

    /* Postcondition */

    /* Figure out which type of box is cheapest compared to how many 
     * marbles they can hold.
     * Then calculate if you can store all marbles in that type. 
     * If not, can the rest be stored in the other type?
     * If not, try storing the marbles in one less of the cheapest box, 
     *  and see if the rest can be in the other type, and continue until 
     *  a possible solution is found. 
     */

    /* Find the cheapest of the two types. Use doubles to be able to calculate 
     * the cheapest in a stituation like c1 = 100, n1 = 30, c2 = 180 and 
     * n2 = 60. With integers, 100/30 = 3 and 180 / 60 = 3. With doubles 
     * 100/30 = 3.3333 and 180/60 is 3.0 */
    value_1 = (double) c1/n1;
    value_2 = (double) c2/n2;
    if (value_1 <= value_2) {
        c_cheap = c1;
        n_cheap = n1;
        c_expensive = c2;
        n_expensive = n2;
    } else {
        c_cheap = c2;
        n_cheap = n2;
        c_expensive = c1;
        n_expensive = n1;
    }

    /* Initialize i outside the if statement, to make i useable afterwards in both cases */
    i = n/n_cheap;
    if (n%n_cheap != 0) {
        /* When not all marbles can be in boxes of the cheapest type, we first try to fill as many boxes of the cheapest type as possible; i = n/n_cheap. Then if the remainding marbles can fit in the other type, thats great, otherwise try with the remainder when we fill i-1 boxes of the cheapest type, either until we find a match, or when i = 0, and no solutions can be found. */
        while(i > 0) {
            if ( (n - n_cheap*i)%n_expensive == 0 ) {
                /* With i boxes of the cheap type, the rest is able to be stored in the expensive type */
                break;
            } else {
                i--;
            }
        }
        if (i == 0) {
            printf("failed\n");
            return 2;
        }
    }

    /* Reverse what type is cheapest to calculate the number of boxes of each type */
    if (n_cheap == n1 && c_cheap == c1) {
        m1 = i;
        m2 = (n - n_cheap*i)/n_expensive;
    } else {
        m2 = i;
        m1 = (n - n_cheap*i)/n_expensive;
    }

    /* Print solution */
    /* Prints ints with up to 10 whitespaces to the left, for aligned output  */
    printf("\n== Number of boxes: ==\n");
    printf("Type 1: %10d\n", m1);
    printf("Type 2: %10d\n", m2);
    printf("\n");
    printf("======= Price: =======\n");
    printf("Type 1: %10d øre\n", m1*c1);
    printf("Type 2: %10d øre\n", m2*c2);
    printf("Total:  %10d øre\n", m1*c1 + m2*c2);

    return 0;
}