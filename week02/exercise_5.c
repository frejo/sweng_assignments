/* 
 * Week 2, exercise 5:
 * 
 * Challenge: What does the following program compute?
 * int x = 0;
 * int y = 0;
 * int h = b - a / 2;
 * while (x <= a)
 * {
 *     printf("(%d,%d)", x, y);
 *     if (h < 0)
 *     {
 *        h += b;
 *     }
 *     else
 *     {
 *         h += b - a;
 *         y++;
 *     }
 *     x++;
 * }
 * Improve the output of the program to support your hypothesis.
 * 
 * Author: Tobias Frejo Rasmussen
 * Answer:
 * 
 * First value (x) will increment by 1 from 0 to a
 * y increments, only when h >= 0
 * 
 * example 1: 
 * a = 4, b = 2
 * 
 * h_0 = 2 - 4/2 = 0        so y increments, and h+= b-a              y_1 = 1
 * h_1 = 0 + 2-4 = -2       so y stays the same, and h_2 = h_1 + b    y_2 = 1
 * h_2 = -2 + 2 = 0         so y increments, and h_3 = h_2 + b-a      y_3 = 2
 * 
 * thus y increments at the same rate as x, and we get 
 * (0,0)(1,1)(2,1)(3,2)(4,2)
 * 
 * example 2:
 * a = 11, b = 3
 * 
 * h_0 = 3 - 11/2 = 3-5 = -2 so y stays the same, and h_1 = h_0 + b y_1 = 0
 * h_1 = -2 + 3 = 1         so y increments                         y_2 = 1
 * h_2 = 1 + 3-11 = -7      y_3 = y_2                               y_3 = 1
 * h_3 = -7+3 = -4          y_4 = y_3                               y_4 = 1
 * h_4 = -4 + 3 = -1        y_5 = y_4                               y_5 = 1
 * h_5 = -1 + 3 = 2         y_6 = h_5 + 1, h_6 = h_5 + b-a          y_6 = 2
 * h_6 = 3 + 3-11 = -6      ...                                     y_7 = 2
 * h_7 = -5 + 3 = -1        ...                                     y_8 = 2
 * h_8 = -2 + 3 = 0         ...                                     y_9 = 3
 * h_9 = -8                 ...                                     y_10 = 3
 * h_10 = -5                ...                                     y_11 = 3
 * loop stops as x = 12
 * 
 * so x goes from 0 to a, while y slowly rises to y_x = b
 * 
 * when b is bigger than a, h will never be below 0, and the output will be:
 * (0,0)(1,1)(2,2)...(a,a)
 * 
 * Author: Tobias Frejo Rasmussen 
 */
#include <stdio.h>

int x = 0;
int y = 0;

int a,b;

void main(void) {
    printf("Enter a and b: ");
    scanf("%d%d", &a, &b);
    int h = b - a / 2;
    while (x <= a)
    {
        printf("h_%d = %d, (%d,%d)\n", x, h, x, y);
        if (h < 0)
        {
            h += b;
        }
        else
        {
            h += b - a;
            y++;
        }
        x++;
    }
    return;
}