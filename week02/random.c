/*
 * Week 2, exercise 2:
 * 
 * Implement a Linear Congruent Random Number Generator computing:
 * x_i+1 = (a ∗ x_i + c) % m
 * ... where m > 0, 0 < a < m, 0 < c < m. The first value, called the seed, is x_0 . Your
 * program should take four integer inputs from the user for a, m, c, x_0 . Given these
 * input values from the user, your program should compute all values x_0 , x_1 , ... , x_m−1
 * and print them to the console as a table in the form:
 * n_0 x_0
 * n_1 x_1
 * ...
 * n_m−1 x_m−1
 * 
 * Answer the following question: What pattern do you observe?
 * 
 * Author: Tobias Frejo Rasmussen
 */

#include <stdio.h>

int a, c, m, x, i;

int main(void) {
    /* Get input */
    printf("Enter seed (x_0): ");
    scanf("%d", &x);
    printf("Enter two positive integers (a, c): ");
    scanf("%d%d", &a, &c);
    printf("Enter int bigger than ");
    if (a > c) printf("%d (m): ", a);
    else printf("%d (m): ", c);
    scanf("%d",&m);

    /* Precondition */
    /* Quit if m > 0, 0 < a < m, 0 < c < m is not true */
    if (m <= 0 || a <= 0 || a >= m || c <= 0 || c >= m ){
        printf("Invalid input!\n");
        return  1;
    }

    /* Postcondition */
    /* Calculate x_i+1 = (a ∗ x_i + c) % m for x_0 to x_m-1 */

    i = 0;
    while (i < m) {
        printf("%d  %d \n", i, x);
        x = (a * x + c) % m;
        i++;
    }

    return 0;

}

/* Observation: A repeating pattern emerges, which will happen when x becomes a previous value of x. */