/*
 * Week 1, exercise 3:
 * 
 * Compute the integer root r of of a number n such that r^2 ≤ n < (r + 1)^2 . Your
 * program should take integer n as input from the user and print the integer root r.
 * An example execution where the user inputs n = 17 could be:
 * 17
 * 4
 * 
 * Author: Tobias Frejo Rasmussen 
 */

#include <stdio.h>

int n, i, j;

int main(void) {
    /* Get input */
    printf("Enter positive integer: ");
    scanf("%d", &n);

    /* Preconditions */
    // Make sure n is positive - we gannot take the root of a negative integer.
    if (n < 0) {
        return 1;
    }

    /* Postconditions */
    // Find the closest perfect square lower or equal to n. 
    // Iterate through all perfect squares until one is bigger than n, and return the previous.

    i = 1;
    while (i*i < n) {
        i++;
    }

    i--;

    /* Print output and exit*/
    printf("Root of the closest lower square is %d (%d² = %d)\n", i,i,i*i );
    return 0;
}