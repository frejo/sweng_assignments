/* 
 * Week 2, exercise 1:
 * 
 * Write a program to compute the Fibonacci numbers. 
 * Your program should take an integer n as input from 
 * the user, and it then calculates and prints each 
 * Fibonaccinumber from the first Fibonacci number 
 * to the nth.  For example, a typical executionwhere t
 * he user inputs n= 7 could be:
 * 7
 * 0 1 1 2 3 5 8
 * 
 * Author: Tobias Frejo Rasmussen
 */
#include <stdio.h>

int n; /* user input */
/* we need to store the last two numbers in the sequence 
 * for the addition. Using unsinged long to be able to get 
 * a longer sequence than with a standard signed int. An 
 * int can't get to an n higher than 47, uint 48, long 93 
 * and unsigned long 94 */
unsigned long last_fibonacci, this_fibonacci; 
int i = 0; /* loop iterative */

int main(void) {
    /* Get input */
    printf("Enter n to print the fobinacci sequence until the nth number.\nn > ");
    scanf("%d", &n);

    /* precondition */
    if (n <= 0) {
        printf("Input n was %d, but cannot be 0 or less.\n", n);
        return 1;
    }

    /* postcondition */
    /* f_n = f_n-2 + f_n-1 */
    last_fibonacci = 0;
    this_fibonacci = 0;

    printf("The first ");
    if (n != 1) /* Print text gramatically correct */
        printf("%d numbers ", n); 
    else 
        printf("number ");
    printf("of the Fibonacci sequence:\n");
    i = 0;
    while (i < n) {
        if (i <= 1) {
            /* The first two loops, i is 0 and 1, which is the same as the fibonacci
             * numbers 1 and 2, and enough info for the program to continue the sequence */
            this_fibonacci = i;
        } else {
            /* At this point, 'this_fibonacci' stores f_n-1 and 'last_fibonacci' stores f_n-2 */
            this_fibonacci = last_fibonacci + this_fibonacci; /* f_n  = f_n-2 + f_n-1 */
            last_fibonacci = this_fibonacci - last_fibonacci; /* f_n-1 = f_n  - f_n-2 */
        }

        i += 1;

        if (last_fibonacci > this_fibonacci) {
            printf("\nInt overflow. Last printed number was the %dth. Quitting.\n", i-1);
            return 2;
        }
        printf("%lu", this_fibonacci); /* %lu is placeholder for unsinged long */

        /* If there are more numbers to print, add a comma and whitespace to seperate the numbers. 
         * Otherwise the program is done, and we end with a newline. */
        if (i < n) {
            printf(", ");
        } else {
            printf("\n");
        }
    }
}