/*
 * Week 2, exercise 4:
 * 
 * What does the following program compute? The result is stored in variable y. Hint:
 * Try printing out the variable values for each iteration (you could try to work out
 * these values by hand with pen and paper, and then by writing and modifying the
 * program by adding printf statements).
 * 
 * int y = 0;
 * int i = 0;
 * int j = 1;
 * int k = 6;
 * while (i < n)
 * {
 *     y += j;
 *     i += 1;
 *     j += k;
 *     k += 6;
 * }
 * 
 * In hand calculation:
 * 
 *  y    i   j  k
 *  0    0   1  6
 *  1    1   7  12
 *  8    2   19 18
 *  27   3   37 24
 *  64   4   61 30
 *  125  5   91 36
 *  216  6  127 42
 *  343  7  169 48
 *  512  8  217 54
 *  729  9  271 60
 * 1000 10  331 66
 * 
 * y hits the powers of 2 1,  8, 64, 512 and 4096 at i=1,2,4,8,16
 *                        2⁰, 2³, 2⁶, 2⁹, 2¹² at i=2⁰,2¹,2²,2³,2⁴
 * Which is y(i) = 2^(log2(i)*3)
 * 
 * Author: Tobias Frejo Rasmussen 
 */

#include <stdio.h>

int y = 0;
int i = 0;
int j = 1;
int k = 6;

int n;

int main(void) {
    scanf("%d", &n);

    printf("   i |    y |    j |    k \n");
    printf("---------------------------\n");
    printf("%4d | %4d | %4d | %4d\n", i,y,j,k);
    while (i < n)
    {
        y += j;
        i += 1;
        j += k;
        k += 6;
        printf("%4d | %4d | %4d | %4d\n", i,y,j,k);
    }
}
