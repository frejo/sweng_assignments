#include <cassert>
#include <iostream>
#include "Clock.h"

int main() {
    /* Test constructors: */
    Clock c1;
    assert(c1.getTime() == 0);

    Clock* c2 = new Clock(0);
    assert(c2->getTime() == 0);

    Clock* c3 = new Clock(10);
    assert(c3->getTime() == 10);

    /* Test single tick */
    c1.tick();
    c2->tick();
    c3->tick();
    assert(c1.getTime() == 1);
    assert(c2->getTime() == 1);
    assert(c3->getTime() == 11);

    /* Test multiple ticks */
    c1.tick(5);
    c2->tick(6);
    c3->tick(7);
    assert(c1.getTime() == 6);
    assert(c2->getTime() == 7);
    assert(c3->getTime() == 18);

    /* Demonstrate alarm */
    c1.setAlarm(10);
    c2->setAlarm(20);
    c3->setAlarm(32);

    for (int i = 0; i < 20; i++) {
        c1.tick();
        c2->tick(2);
        c3->tick(3);
    }

    std::cout << std::endl << "End times:" << std::endl;
    std::cout << "  c1: " << c1.getTime() << std::endl;
    std::cout << "  c2: " << c2->getTime() << std::endl;
    std::cout << "  c3: " << c3->getTime() << std::endl;

    delete c2;
    delete c3;

    return 0;
}