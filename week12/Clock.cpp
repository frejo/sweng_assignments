#include <cassert>
#include <iostream>
#include "Clock.h"

Clock::Clock() {
    time = 0;
    alarm = 0;
    alarmHasBeenSet = false;
}

Clock::Clock(int t) {
    assert(t >= 0);
    time = t;
    alarm = 0;
    alarmHasBeenSet = false;
}

Clock::~Clock() {
}

int Clock::getTime() {
    return time;
}

void Clock::checkAndUpdateAlarm() {
    if (alarmHasBeenSet && time >= alarm) {
        std::cout << "ALARM AT " << alarm << ". TIME IS " << time << std::endl;
        alarmHasBeenSet = false;
    }
}

void Clock::tick() {
    tick(1);
}

void Clock::tick(int dt) {
    assert(dt >= 0);
    time += dt;
    checkAndUpdateAlarm();
}

void Clock::setAlarm(int a) {
    assert(a > time);
    alarm = a;
    alarmHasBeenSet = true;
}