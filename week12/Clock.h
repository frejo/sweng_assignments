
#ifndef _CLOCK_CLASS_H_
#define _CLOCK_CLASS_H_

class Clock {
  private:
    int time;
    int alarm;
    bool alarmHasBeenSet;

    void checkAndUpdateAlarm();

  public:
    Clock();
    Clock(int initial_time);
    ~Clock();
    
    int getTime();
    void tick();
    void tick(int dt);
    void setAlarm(int a);
};

#endif